<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<body>
    <p>This is an unprotected page, anyone can see it</p>
    <p>This is a secured page which retrieves your details from Cloudseal: <a href="<c:url value="protected/about_me" />">about me</a></p>
    <p>This page is only available to admins: <a href="<c:url value="protected/admin" />">admins only</a></p>
</body>
</html>
