<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>Admin page</title>
</head>
<body>
    <h2>Admin Page</h2>
    <a href="<c:url value='/j_spring_security_logout' />">logout</a>
    <p>This page is visible to admins only (those with ROLE_ADMIN role)</p>
    <p><a href="<c:url value='/' />">home</a></p>
</body>
</html>