<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>About Me</title>
</head>
<body>
    <h2>About Me</h2>
    <a href="<c:url value='/j_spring_security_logout' />">logout</a>
    ${userInformation}
    <p><a href="<c:url value='/' />">home</a> <br /></p>
</body>
</html>