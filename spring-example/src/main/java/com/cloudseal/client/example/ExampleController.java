/* Copyright 2012 Cloudseal Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cloudseal.client.example;

import com.cloudseal.client.spring.CloudsealUserDetails;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import java.security.Principal;
import java.util.*;

@Controller
public class ExampleController {

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(value = "/about_me", method = RequestMethod.GET)
    public ModelAndView aboutMe(Principal principal) throws Exception {
        CloudsealUserDetails userDetails = (CloudsealUserDetails) ((Authentication) principal).getPrincipal();

        StringBuilder builder = new StringBuilder();
        builder.append("<p>\n");
        Map<String, Object> props = BeanUtils.describe(userDetails);
        props.remove("attributes");
        props.remove("roles");
        props.remove("class");
        props.remove("authorities");
        props.remove("accountNonExpired");
        props.remove("accountNonLocked");
        props.remove("credentialsNonExpired");

        List<String> keys = asSortedList(props.keySet());
        Iterator<String> iterator = keys.iterator();

        while (iterator.hasNext()) {
            String key = iterator.next();
            builder.append("<strong>" + key + ":</strong> " + props.get(key) + "<br />" + "\n");
        }
        builder.append("</p>\n");

        builder.append("<p>\n");
        builder.append("<strong>roles:</strong> ");
        for (GrantedAuthority role : userDetails.getAuthorities()) {
            builder.append(role.getAuthority() + " ");
        }
        builder.append("</p>\n");

        ModelAndView mav = new ModelAndView("protected/about_me");
        mav.addObject("userInformation", builder.toString());
        return mav;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public ModelAndView adminsOnly() {
        return new ModelAndView("protected/admins_only");
    }

    private <T extends Comparable<? super T>> List<T> asSortedList(Collection<T> c) {
        List<T> list = new ArrayList<T>(c);
        java.util.Collections.sort(list);
        return list;
    }

}
