/**
 * Unchecked exception classes that could be thrown during REST calls
 *
 * @since 1.0
 */
package com.cloudseal.rest.exception;