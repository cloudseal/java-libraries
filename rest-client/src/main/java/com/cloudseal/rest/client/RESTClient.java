/* Copyright 2012 Cloudseal Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cloudseal.rest.client;

/**
 * Central interface for executing REST/RPC requests against the Cloudseal
 * platform. 
 * 
 * Generally it should not by used directly but instead passed to a
 * service (e.g. the {@link UserService}) which will use it to execute requests
 * 
 * @author Toby Hobson <a href="mailto:toby.hobson@cloudseal.com">toby.hobson@cloudseal.com</a>
 * @since 1.0
 * 
 */
public interface RESTClient {

	/**
	 * Invoke a GET request against the URL. 
	 * Note: implementations will prepend https://&lt;&lt;hostname&gt;&gt;.cloudseal.com to the path
	 * 
	 * @param path e.g. /rest/user/testuser
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public abstract <T> T get(String path);

	/**
	 * Invoke a POST request against the URL. 
	 * Note: implementations will prepend https://&lt;&lt;hostname&gt;&gt;.cloudseal.com to the path
	 * 
	 * @param path e.g. /rest/user
	 * @param body Jaxb object to be sent
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public abstract <T> T post(String path, T body);

	/**
	 * Invoke a PUT request against the URL. 
	 * Note: implementations will prepend https://&lt;&lt;hostname&gt;&gt;.cloudseal.com to the path
	 * 
	 * @param path e.g. /rest/user/testuser
	 * @param body Jaxb object to be sent
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public abstract <T> T put(String path, T body);

	/**
	 * Invoke a DELETE request against the URL. 
	 * Note: implementations will prepend https://&lt;&lt;hostname&gt;&gt;.cloudseal.com to the path
	 * 
	 * @param path e.g. /rest/user/testuser
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public abstract <T> T delete(String path);

}