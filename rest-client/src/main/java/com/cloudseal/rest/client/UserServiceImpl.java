/* Copyright 2012 Cloudseal Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cloudseal.rest.client;

import com.cloudseal.rest.exception.*;
import com.cloudseal.rest.jaxb.ChangePassword;
import com.cloudseal.rest.jaxb.CloudsealUser;

/**
 * Simple implementation of the {@link com.cloudseal.rest.client.UserService}
 *
 * @author Toby Hobson <a href="mailto:toby.hobson@cloudseal.com">toby.hobson@cloudseal.com</a>
 * @since 1.0
 */
public class UserServiceImpl implements UserService {

	private RESTClient restClient;

	public UserServiceImpl(RESTClient restClient) {
		this.restClient = restClient;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cloudseal.rest.client.UserService#getUser(java.lang.String)
	 */
	@Override
	public CloudsealUser getUser(String username) {
		StringBuilder builder = new StringBuilder("/rest/user/");
		builder.append(username);
		CloudsealUser user = restClient.get(builder.toString());
		return user;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cloudseal.rest.client.UserService#addUser(com.cloudseal.rest.jaxb
	 * .CloudsealUser)
	 */
	@Override
	public CloudsealUser addUser(CloudsealUser user) {
		try {
			CloudsealUser newUser = restClient.post("/rest/user", user);
			return newUser;
		} catch (RestException ex) {
			if (ex.getCause() != null && ex.getCause().getMessage().contains("DUPLICATE_USER")) {
				throw new DuplicateUserException(ex.getCause().getMessage());
			}
			if (ex.getCause() != null && ex.getCause().getMessage().contains("VIOLATION")) {
				throw new ValidationException(ex.getCause().getMessage());
			}
			throw ex;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.cloudseal.rest.client.UserService#updateUser(com.cloudseal.rest.jaxb
	 * .CloudsealUser)
	 */
	@Override
	public CloudsealUser updateUser(CloudsealUser user) {
		try {
			StringBuilder builder = new StringBuilder("/rest/user/");
			builder.append(user.getUsername());
			CloudsealUser updatedUser = restClient.put(builder.toString(), user);
			return updatedUser;
		} catch (RestException ex) {
			if (ex.getCause() != null && ex.getCause().getMessage().contains("USER_NOT_FOUND")) {
				throw new UserNotFoundException(ex.getCause().getMessage());
			}
			if (ex.getCause() != null && ex.getCause().getMessage().contains("VIOLATION")) {
				throw new ValidationException(ex.getCause().getMessage());
			}
			throw ex;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cloudseal.rest.client.UserService#deleteUser(java.lang.String)
	 */
	@Override
	public void deleteUser(String username) {
		try {
			StringBuilder builder = new StringBuilder("/rest/user/");
			builder.append(username);
			restClient.delete(builder.toString());
		} catch (RestException ex) {
			if (ex.getCause() != null && ex.getCause().getMessage().contains("USER_NOT_FOUND")) {
				throw new UserNotFoundException(ex.getCause().getMessage());
			}
			throw ex;
		}
	}

    @Override
    public void changePassword(String username, String newPassword) {
        try {
            StringBuilder builder = new StringBuilder("/rest/change_password/");
            builder.append(username);
            ChangePassword changePassword = new ChangePassword();
            changePassword.setNewPassword(newPassword);
            restClient.put(builder.toString(), changePassword);
        } catch (RestException ex) {
            if (ex.getCause() != null && ex.getCause().getMessage().contains("USER_NOT_FOUND")) {
                throw new UserNotFoundException(ex.getCause().getMessage());
            }
            throw ex;
        }
    }

    @Override
    public void changePassword(String username, String currentPassword, String newPassword) {
        if (newPassword == null || newPassword.length() < 1) {
            throw new InvalidPasswordException("INVALID PASSWORD");
        }

        try {
            StringBuilder builder = new StringBuilder("/rest/change_password/");
            builder.append(username);
            ChangePassword changePassword = new ChangePassword();
            changePassword.setCurrentPassword(currentPassword);
            changePassword.setNewPassword(newPassword);
            restClient.put(builder.toString(), changePassword);
        } catch (RestException ex) {
            if (ex.getCause() != null && ex.getCause().getMessage().contains("USER_NOT_FOUND")) {
                throw new UserNotFoundException(ex.getCause().getMessage());
            }
            if (ex.getCause() != null && ex.getCause().getMessage().contains("INVALID_PASSWORD")) {
                throw new InvalidPasswordException(ex.getCause().getMessage());
            }
            throw ex;
        }
    }

}
