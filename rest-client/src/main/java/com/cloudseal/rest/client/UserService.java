/* Copyright 2012 Cloudseal Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cloudseal.rest.client;

import com.cloudseal.rest.jaxb.CloudsealUser;

/**
 * Central interface for managing users, uses a {@link com.cloudseal.rest.client.RESTClient}
 * 
 * @author Toby Hobson <a href="mailto:toby.hobson@cloudseal.com">toby.hobson@cloudseal.com</a>
 * @since 1.0
 *
 */
public interface UserService {

	/**
	 * Retrieve a user by username
	 * 
	 * @param username Cloudseal username
	 * @return Cloudseal user or null if no user exists
	 */
	public abstract CloudsealUser getUser(String username);

	/**
	 * Add new user
	 * 
	 * @param user User to be added
	 * 
	 * @return Updated user
	 * @throws com.cloudseal.rest.exception.DuplicateUserException
	 * @throws com.cloudseal.rest.exception.ValidationException If validation constraints are not met
	 */
	public abstract CloudsealUser addUser(CloudsealUser user);

	/**
	 * Update user
	 * 
	 * @param user User to be updated
	 * @return Updated user
	 * @throws com.cloudseal.rest.exception.ValidationException If validation constraints are not met
	 * @throws com.cloudseal.rest.exception.UserNotFoundException
	 */
	public abstract CloudsealUser updateUser(CloudsealUser user);

	/**
	 * Delete user 
	 * 
	 * @param username Cloudseal username
	 * @throws com.cloudseal.rest.exception.UserNotFoundException
	 */
	public abstract void deleteUser(String username);

    /**
     * Change user password
     *
     * @params username Cloudseal username
     * @throws com.cloudseal.rest.exception.UserNotFoundException
     */
    public abstract void changePassword(String username, String newPassword);

    /**
     * Change user password, but also validate that the current password is correct. This method may be used
     * when you want to prompt the user to enter his current password and only change the password if the
     * password supplied is valid
     *
     * @params username Cloudseal username
     * @throws com.cloudseal.rest.exception.UserNotFoundException
     * @throws com.cloudseal.rest.exception.InvalidPasswordException
     */
    public abstract void changePassword(String username, String currentPassword, String newPassword);

}