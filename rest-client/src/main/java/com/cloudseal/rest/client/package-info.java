/**
 * Core interfaces and classes for making requests against the Cloudseal REST api
 *
 * @since 1.0
 */
package com.cloudseal.rest.client;