/* Copyright 2012 Cloudseal Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cloudseal.rest;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import com.cloudseal.rest.jaxb.CloudsealUser;
import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicStatusLine;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.cloudseal.rest.client.RESTClientImpl;
import com.cloudseal.rest.exception.RestException;

@RunWith(MockitoJUnitRunner.class)
public class RESTClientTest {

	private RESTClientImpl classUnderTest;
	
	@Mock private HttpClient httpClient;
	
	@Before
	public void setup() {
		classUnderTest = new RESTClientImpl("test", "a", "b", httpClient);
	}
	
	@Test
	public void testGetUser() throws IOException {
		String responseXml = getXml("xml/GetUser.xml");
		HttpEntity responseBody = new StringEntity(responseXml);
		StatusLine statusLine = new BasicStatusLine(HttpVersion.HTTP_1_1, 200, null);
		HttpResponse response = mock(HttpResponse.class);
		when(response.getEntity()).thenReturn(responseBody);
		when(response.getStatusLine()).thenReturn(statusLine);
		when(response.getAllHeaders()).thenReturn(buildHeaders());
		when(httpClient.execute(any(HttpUriRequest.class))).thenReturn(response);
		
		CloudsealUser user = classUnderTest.get("/rest/user/test");
		assertEquals("test", user.getUsername());
		assertEquals("test", user.getFirstName());
		assertEquals("user", user.getLastName());
		assertEquals("password", user.getPassword());
	}
	
	@Test
	public void testGetNullUser() throws IOException {
		String responseXml = getXml("xml/GetUser.xml");
		HttpEntity responseBody = new StringEntity(responseXml);
		StatusLine statusLine = new BasicStatusLine(HttpVersion.HTTP_1_1, 404, "USER_NOT_FOUND");
		HttpResponse response = mock(HttpResponse.class);
		when(response.getEntity()).thenReturn(responseBody);
		when(response.getStatusLine()).thenReturn(statusLine);
		when(response.getAllHeaders()).thenReturn(buildHeaders());
		when(httpClient.execute(any(HttpUriRequest.class))).thenReturn(response);
		
		CloudsealUser user = classUnderTest.get("/rest/user/test");
		assertNull(user);
	}
	
	@Test
	public void testPostUser() throws IOException {
		String responseXml = getXml("xml/GetUser.xml");
		HttpEntity responseBody = new StringEntity(responseXml);
		StatusLine statusLine = new BasicStatusLine(HttpVersion.HTTP_1_1, 200, null);
		HttpResponse response = mock(HttpResponse.class);
		when(response.getEntity()).thenReturn(responseBody);
		when(response.getStatusLine()).thenReturn(statusLine);
		when(response.getAllHeaders()).thenReturn(buildHeaders());
		when(httpClient.execute(any(HttpUriRequest.class))).thenReturn(response);
		
		CloudsealUser newUser = new CloudsealUser();
		newUser.setUsername("test");
		newUser.setFirstName("test");
		newUser.setLastName("user");
		newUser.setEmail("test@test.com");
		
		CloudsealUser user = classUnderTest.post("/rest/users", newUser);
		assertEquals("test", user.getUsername());
		assertEquals("test", user.getFirstName());
		assertEquals("user", user.getLastName());
		assertEquals("password", user.getPassword());
	}
	
	@Test
	public void testUpdateUser() throws IOException {
		String responseXml = getXml("xml/GetUser.xml");
		HttpEntity responseBody = new StringEntity(responseXml);
		StatusLine statusLine = new BasicStatusLine(HttpVersion.HTTP_1_1, 200, null);
		HttpResponse response = mock(HttpResponse.class);
		when(response.getEntity()).thenReturn(responseBody);
		when(response.getStatusLine()).thenReturn(statusLine);
		when(response.getAllHeaders()).thenReturn(buildHeaders());
		when(httpClient.execute(any(HttpUriRequest.class))).thenReturn(response);
		
		CloudsealUser newUser = new CloudsealUser();
		newUser.setUsername("test");
		newUser.setFirstName("test");
		newUser.setLastName("user");
		newUser.setEmail("test@test.com");
		
		CloudsealUser user = classUnderTest.put("/rest/user/test", newUser);
		assertEquals("test", user.getUsername());
		assertEquals("test", user.getFirstName());
		assertEquals("user", user.getLastName());
		assertEquals("password", user.getPassword());
	}
	
	@Test
	public void testUpdateNullUser() throws IOException {
		StatusLine statusLine = new BasicStatusLine(HttpVersion.HTTP_1_1, 404, "USER_NOT_FOUND");
		HttpResponse response = mock(HttpResponse.class);
		when(response.getStatusLine()).thenReturn(statusLine);
		when(response.getAllHeaders()).thenReturn(buildHeaders());
		when(httpClient.execute(any(HttpUriRequest.class))).thenReturn(response);
		
		CloudsealUser newUser = new CloudsealUser();
		newUser.setUsername("test");
		newUser.setFirstName("test");
		newUser.setLastName("user");
		newUser.setEmail("test@test.com");
		
		try {
			classUnderTest.put("/rest/user/test", newUser);
			fail("Expected exception");
		} catch (RestException ex) {
			String message = ex.getCause().getMessage();
			assertEquals("USER_NOT_FOUND", message);
		}
	}
	
	@Test
	public void testDeleteUser() throws IOException {
		String responseXml = getXml("xml/GetUser.xml");
		HttpEntity responseBody = new StringEntity(responseXml);
		StatusLine statusLine = new BasicStatusLine(HttpVersion.HTTP_1_1, 200, null);
		HttpResponse response = mock(HttpResponse.class);
		when(response.getEntity()).thenReturn(responseBody);
		when(response.getStatusLine()).thenReturn(statusLine);
		when(response.getAllHeaders()).thenReturn(buildHeaders());
		when(httpClient.execute(any(HttpUriRequest.class))).thenReturn(response);
		
		CloudsealUser user = classUnderTest.delete("/rest/user/test");
		assertEquals("test", user.getUsername());
		assertEquals("test", user.getFirstName());
		assertEquals("user", user.getLastName());
		assertEquals("password", user.getPassword());
	}
	
	@Test
	public void testDeleteNullUser() throws IOException {
		StatusLine statusLine = new BasicStatusLine(HttpVersion.HTTP_1_1, 404, "USER_NOT_FOUND");
		HttpResponse response = mock(HttpResponse.class);
		when(response.getStatusLine()).thenReturn(statusLine);
		when(response.getAllHeaders()).thenReturn(buildHeaders());
		when(httpClient.execute(any(HttpUriRequest.class))).thenReturn(response);
		
		try {
			classUnderTest.delete("/rest/user/test");
			fail("Expected exception");
		} catch (RestException ex) {
			String message = ex.getCause().getMessage();
			assertEquals("USER_NOT_FOUND", message);
		}
	}
	
	private String getXml(String path) throws IOException {
		InputStream in = this.getClass().getClassLoader().getResourceAsStream(path);
		StringWriter out = new StringWriter();
		IOUtils.copy(in, out);
		IOUtils.closeQuietly(in);
		IOUtils.closeQuietly(out);
		String xml = out.toString();
		return xml;
	}
	
	private Header[] buildHeaders() {
		Header lang = new BasicHeader("Content-Language", "en-US");
		Header type = new BasicHeader("Content-Type", "application/xml");
		Header[] headers = new Header[] { lang, type };
		return headers;
	}
}
