package com.cloudseal.test;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: toby
 * Date: 21/01/2013
 * Time: 17:54
 * To change this template use File | Settings | File Templates.
 */
public class LogoutServlet extends HttpServlet {

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getSession().invalidate();
        resp.getWriter().println("<html><body>");
        String contextRoot = req.getContextPath();
        resp.getWriter().print("<p><a href=\"" + contextRoot + "\">home</a></p>");
        resp.getWriter().println("<p>Logged out</p>");
        resp.getWriter().println("</body></html>");
    }

}
