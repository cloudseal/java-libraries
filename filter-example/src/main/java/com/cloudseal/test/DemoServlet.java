/* Copyright 2012 Cloudseal Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cloudseal.test;

import com.cloudseal.client.filter.CloudsealUtils;
import com.cloudseal.client.saml2.CloudsealPrincipal;
import org.apache.commons.beanutils.BeanUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

public class DemoServlet extends HttpServlet {

    @Override
    @SuppressWarnings("unchecked")
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // Get the Cloudseal principal
        CloudsealPrincipal cloudsealPrincipal = CloudsealUtils.getPrincipal(req);

        String contextRoot = req.getContextPath();
        resp.getWriter().println("<html><body>");
        resp.getWriter().println("<p><a href=\"" + contextRoot + "/logout\">logout</a></p>");
        resp.getWriter().println("<h3>User Properties</h3>");

        // Here we're using commons BeanUtils to create a map of all the properties on the CloudsealPrincipal
        // so we can display them
        try {
            Map<String, Object> props = new TreeMap(BeanUtils.describe(cloudsealPrincipal));
            props.remove("attributes");
            props.remove("roles");
            props.remove("class");

            Iterator<String> iterator =  props.keySet().iterator();

            while (iterator.hasNext()) {
                String key = iterator.next();
                resp.getWriter().println("<strong>" + key + ":</strong> " + props.get(key) + "<br />");
            }

            String[] roles = BeanUtils.getArrayProperty(cloudsealPrincipal, "roles");
            resp.getWriter().print("<strong>roles: </strong>");
            for (String role : roles) {
                resp.getWriter().print(role + " ");
            }
            resp.getWriter().print("<br />");

        } catch (Exception ex) {
            throw new ServletException(ex);
        }

        // Just illustrates that even after a redirect to Cloudseal the original request parameters are preserved
        resp.getWriter().println("<h3>Request parameters</h3>");

        Map params = req.getParameterMap();
        Set<String> keys = params.keySet();

        for (String key : keys) {
            String[] value = (String[]) params.get(key);
            resp.getWriter().println(key + " : " + value[0] + "<br />");
        }

        resp.getWriter().println("</body></html>");
        resp.getWriter().flush();
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getWriter().println("hello world");
        // Just illustrates that even after a redirect to Cloudseal the original request parameters are preserved
        Map params = req.getParameterMap();
        Set<String> keys = params.keySet();

        for (String key : keys) {
            String[] value = (String []) params.get(key);
            resp.getWriter().println(key + " : " + value[0]);
        }
        resp.getWriter().flush();
    }

}