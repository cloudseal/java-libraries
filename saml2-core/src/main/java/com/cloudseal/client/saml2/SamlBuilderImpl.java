/* Copyright 2012 Cloudseal Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cloudseal.client.saml2;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.net.URLEncoder;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Signature;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.zip.Deflater;

/**
 * Central class responsible for generating the SAML AuthnRequest.
 * <strong>Note: the init() method must be called before using this class</strong>
 */
public class SamlBuilderImpl implements SamlBuilder {

    private static Logger logger = Logger.getLogger(SamlBuilderImpl.class);

    public static final String SAML_NAMESPACE = "urn:oasis:names:tc:SAML:2.0:assertion";
    public static final String SAML_PROTOCOL_NAMESPACE = "urn:oasis:names:tc:SAML:2.0:protocol";
    public static final String TRANSIENT_NAME_ID_POLICY = "urn:oasis:names:tc:SAML:2.0:nameid-format:transient";
    public static final String RSA_SIGNATURE_ALGORITHM = "http://www.w3.org/2000/09/xmldsig#rsa-sha1";
    public static final String DSA_SIGNATURE_ALGORITHM = "http://www.w3.org/2000/09/xmldsig#dsa-sha1";
    public static final String REPLY_BINDING = "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST";

    private TransformerFactory transformerFactory;
    private String providerName = null;
    private KeyStore.PrivateKeyEntry privateKey = null;
    private String sigAlg;
    private String acsPath;

    /**
     * <p>Primrily responsible for loading the keys from the keystore. A flag will be set to indicate the type
     * of key used (RSA or DSA) and this flag will subsequently be read by the generateAuthRequest() method
     * when sending the request.</p>
     *
     * <p><strong>Note: all properties of the AuthRequestContext except providerName must be set</strong></p>
     *
     * @param context
     * @throws Exception
     */
    @Override
    public synchronized void init(SamlConfig context) throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Initializing " + SamlBuilderImpl.class.getSimpleName());
        }
        transformerFactory = TransformerFactory.newInstance();
        providerName = context.getProviderName();

        // Keystore
        byte[] keystore = context.getKeystore();

        InputStream is = new ByteArrayInputStream(keystore);
        try {
            KeyStore ks = KeyStore.getInstance("JKS");
            ks.load(is, context.getKeystorePassword().toCharArray());
            privateKey = (KeyStore.PrivateKeyEntry)
                    ks.getEntry(context.getKeyName(),
                            new KeyStore.PasswordProtection(context.getKeyPassword().toCharArray()));

            if (privateKey == null) {
                throw new MissingKeyException("Unable to find key named " + context.getKeyName() + " in keystore");
            }

            privateKey.getPrivateKey();
            PrivateKey pk = privateKey.getPrivateKey();
            String algorithm = pk.getAlgorithm();

            if ("RSA".equals(algorithm)) {
                sigAlg = RSA_SIGNATURE_ALGORITHM;
            } else if ("DSA".equals(algorithm)) {
                sigAlg = DSA_SIGNATURE_ALGORITHM;
            } else {
                throw new Exception("Unsupported key algorithm: " + algorithm);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        finally {
            if (is != null) {
                is.close();
            }
        }

    }

    /**
     * <p>Generate the SAML request using the redirect binding. The method performs several steps as required by the Redirect Binding spec:</p>
     *
     * <ol>
     *     <li>Generate the AuthnRequest xml message</li>
     *     <li>Deflate (zip) it</li>
     *     <li>Base64Encode it</li>
     *     <li>URLEncode it</li>
     *     <li>Generate the full request string i.e. SAMLRequest=x&amp;SigAlg=y</li>
     *     <li>Sign the request string</li>
     *     <li>Base64 and URLEncode the signature</li>
     *     <li>Append the signature to the request string</li>
     * </ol>
     *
     * @param spIssuer
     * @param acsUrl This is important, it indicates the URL to which the IDP should post the Auth Response.
     * @param destination
     * @param id This is also important, it allows us to correlate requests and responses because the IDP will send a response inResponseTo this id
     * @return
     * @throws Exception
     */
    @Override
    public String generateSamlAuthRequest(String spIssuer, String acsUrl, String destination, String id) throws Exception {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setNamespaceAware(true);

        DocumentBuilder docBuilder = documentBuilderFactory.newDocumentBuilder();
        Document doc = docBuilder.newDocument();

        Node authRequest = buildAuthRequest(doc, acsUrl, destination, id, spIssuer, providerName);
        doc.appendChild(authRequest);

        String xmlString = marshallDocument(doc);
        if (logger.isDebugEnabled()) {
            String message = "AuthnRequest: " + xmlString;
            logger.debug(message);
        }

        byte[] deflatedXml = deflate(xmlString);
        String base64EncodedXml = base64Encode(deflatedXml);
        String urlEncodedXml = urlEncode(base64EncodedXml);

        String url = buildRedirectUrl(urlEncodedXml, sigAlg);

        String signature = signRequest(url);

        StringBuilder builder = new StringBuilder(url);
        builder.append("&Signature=");
        builder.append(signature);

        return builder.toString();
    }

    protected Node buildAuthRequest(Document document, String acsUrl, String destination,
                                  String id, String spIssuer, String providerName) {
        Element issuer = document.createElementNS(SAML_NAMESPACE, "Issuer");
        issuer.setPrefix("saml");
        issuer.appendChild(document.createTextNode(spIssuer));

        Element nameIDPolicy = document.createElementNS(SAML_PROTOCOL_NAMESPACE, "NameIDPolicy");
        nameIDPolicy.setPrefix("samlp");
        nameIDPolicy.setAttribute("AllowCreate", "true");
        nameIDPolicy.setAttribute("Format", TRANSIENT_NAME_ID_POLICY);

        Element authRequest = document.createElementNS(SAML_PROTOCOL_NAMESPACE, "AuthnRequest");
        authRequest.setPrefix("samlp");
        authRequest.setAttribute("ID", id);
        authRequest.setAttribute("IssueInstant", generateIssueDate());
        authRequest.setAttribute("AssertionConsumerServiceURL", acsUrl);
        authRequest.setAttribute("Destination", destination);
        authRequest.setAttribute("IsPassive", "false");
        authRequest.setAttribute("ProtocolBinding", REPLY_BINDING);
        authRequest.setAttribute("ProviderName", providerName);
        authRequest.setAttribute("Version", "2.0");

        authRequest.appendChild(issuer);
        authRequest.appendChild(nameIDPolicy);

        return authRequest;
    }

    protected String generateIssueDate() {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
        df.setTimeZone(tz);
        String nowAsISO = df.format(new Date());
        return nowAsISO;
    }

    protected String marshallDocument(Document doc) throws TransformerException {
        Transformer trans = transformerFactory.newTransformer();
        trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        trans.setOutputProperty(OutputKeys.INDENT, "yes");

        //create string from xml tree
        StringWriter sw = new StringWriter();
        StreamResult result = new StreamResult(sw);
        DOMSource source = new DOMSource(doc);
        trans.transform(source, result);

        return sw.toString();
    }

    protected byte[] deflate(String xml) throws UnsupportedEncodingException {
        // Compress the bytes
        byte[] buffer = new byte[1024];
        Deflater compresser = new Deflater(Deflater.DEFAULT_COMPRESSION, true);
        compresser.setInput(xml.getBytes("UTF-8"));
        compresser.finish();
        int compressedDataLength = compresser.deflate(buffer);
        byte[] output = new byte[compressedDataLength];
        for (int i=0; i<compressedDataLength; i++) {
            output[i] = buffer[i];
        }
        compresser.reset();
        return output;
    }

    protected String base64Encode(byte[] data) {
        String encodedString = Base64.encodeBase64String(data);
        // Remove whitespace from string
        encodedString = encodedString.replaceAll("\\s","");
        return encodedString;
    }

    protected String urlEncode(String text) throws Exception {
        return URLEncoder.encode(text, "UTF-8");
    }

    protected String buildRedirectUrl(String samlRequest, String sigAlg) throws Exception {
        StringBuilder builder = new StringBuilder();
        builder.append("SAMLRequest=");
        builder.append(samlRequest);
        builder.append("&SigAlg=");
        builder.append(urlEncode(sigAlg));
        return builder.toString();
    }

    protected String signRequest(String request) throws Exception {
        Signature signature;
        if (RSA_SIGNATURE_ALGORITHM.equals(sigAlg)) {
            signature = Signature.getInstance("SHA1withRSA");
        } else if (DSA_SIGNATURE_ALGORITHM.equals(sigAlg)) {
            signature = Signature.getInstance("SHA1withDSA");
        } else {
            throw new Exception("Unsupported key algorithm: " + sigAlg);
        }

        signature.initSign(privateKey.getPrivateKey());
        signature.update(request.getBytes("UTF-8"));

        byte[] signatureBytes = signature.sign();
        String base64EncodedSignature = base64Encode(signatureBytes);
        return urlEncode(base64EncodedSignature);
    }

    /**
     * Not required for the redirect binding because we sign the deflated and encoded url, not
     * the document itself but worth keeping here anyway in case we use the post or artefact bindings
     *
     * @param signature
     * @return
     * @throws Exception
     */
    protected String generateSignature(Node signature) throws Exception {
        TransformerFactory transfac = TransformerFactory.newInstance();
        Transformer trans = transfac.newTransformer();
        trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        trans.setOutputProperty(OutputKeys.INDENT, "yes");

        //create string from xml tree
        StringWriter sw = new StringWriter();
        StreamResult result = new StreamResult(sw);
        DOMSource source = new DOMSource(signature);
        trans.transform(source, result);
        String signatureString = sw.toString();

        String base64Signature = base64Encode(signatureString.getBytes("UTF-8"));
        String urlEncodedSignature = urlEncode(base64Signature);
        return urlEncodedSignature;
    }

}
