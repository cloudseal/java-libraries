package com.cloudseal.client.saml2;

import java.security.PublicKey;

/**
 * Created with IntelliJ IDEA.
 * User: toby
 * Date: 11/12/2012
 * Time: 17:28
 * To change this template use File | Settings | File Templates.
 */
public interface SamlValidator {

    CloudsealPrincipal validateAuthResponse(PublicKey idpPublicKey, String encodedResponse, String expectedInResponseTo, String expectedAudience) throws VerificationException;
    void validateLogoutRequest(PublicKey idpPublicKey, String queryString) throws VerificationException;
}
