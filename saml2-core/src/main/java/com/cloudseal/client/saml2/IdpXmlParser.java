/* Copyright 2012 Cloudseal Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cloudseal.client.saml2;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;

public class IdpXmlParser {

    private static Logger logger = Logger.getLogger(SamlBuilderImpl.class);

    private String ssoUrl = null;
    private String sloUrl = null;
    private PublicKey idpPublicKey = null;

    public void parse(byte[] idp) throws Exception {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setNamespaceAware(true);

        // IDP xml
        if (logger.isDebugEnabled()) {
            logger.debug("Parsing IDP xml");
        }

        ByteArrayInputStream bis = null;
        try {
            bis = new ByteArrayInputStream(idp);
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document idpXmlDom = documentBuilder.parse(bis);
            idpPublicKey = getPublicKey(idpXmlDom);
            ssoUrl = getSsoUrl(idpXmlDom);
            sloUrl = getSloUrl(idpXmlDom);
        } finally {
            if (bis != null) {
                bis.close();
            }
        }
    }

    private PublicKey getPublicKey(Document document) throws Exception {
        NodeList nl = document.getElementsByTagNameNS(XMLSignature.XMLNS, "X509Certificate");
        if (nl.getLength() == 0) {
            logger.error("Unable to find X509Signature element in IDP.xml");
            throw new Exception("Unable to retrieve certificate from IDP.xml");
        }

        Node certificateNode = nl.item(0);

        String certificateText = certificateNode.getTextContent();
        if (certificateText == null || certificateText.length() < 1) {
            logger.error("X509Signature element in IDP.xml is empty");
            throw new Exception("Unable to retrieve certificate from IDP.xml");
        }

        InputStream is = null;
        try {
            byte[] certData = Base64.decodeBase64(certificateText.getBytes("UTF-8"));
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            is = new ByteArrayInputStream(certData);
            Certificate cert = cf.generateCertificate(is);
            PublicKey publicKey = cert.getPublicKey();
            return publicKey;
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private String getSsoUrl(Document document) throws Exception {
        NodeList nl = document.getElementsByTagNameNS("urn:oasis:names:tc:SAML:2.0:metadata", "SingleSignOnService");
        if (nl.getLength() == 0) {
            logger.error("Unable to find SingleSignOnService element in IDP.xml");
            throw new Exception("Unable to find SingleSignOnService element in IDP.xml");
        }
        Element element = (Element) nl.item(0);
        String ssoUrl = element.getAttribute("Location");
        return ssoUrl;
    }

    private String getSloUrl(Document document) throws Exception {
        NodeList nl = document.getElementsByTagNameNS("urn:oasis:names:tc:SAML:2.0:metadata", "SingleLogoutService");
        if (nl.getLength() == 0) {
            logger.error("Unable to find SingleLogoutService element in IDP.xml");
            throw new Exception("Unable to find SingleLogoutService element in IDP.xml");
        }
        Element element = (Element) nl.item(0);
        String ssoUrl = element.getAttribute("Location");
        return ssoUrl;
    }

    public String getSsoUrl() {
        return ssoUrl;
    }

    public String getSloUrl() {
        return sloUrl;
    }

    public PublicKey getIdpPublicKey() {
        return idpPublicKey;
    }
}
