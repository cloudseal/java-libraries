/* Copyright 2012 Cloudseal Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cloudseal.client.saml2;

import java.io.Serializable;
import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class CloudsealPrincipal implements Principal, Serializable {

    protected Map<String, Object> attributes;
    protected Set<String> roles;

    public CloudsealPrincipal() {
        attributes = new HashMap<String, Object>();
        roles = new HashSet<String>();
    }

    public String getName() {
        return (String) attributes.get("username");
    }

    public String getAddress() {
        return (String) attributes.get("address");
    }

    public String getPostCode() {
        return (String) attributes.get("postCode");
    }

    public String getPhone() {
        return (String) attributes.get("phone");
    }

    public String getCompany() {
        return (String) attributes.get("company");
    }

    public String getJobTitle() {
        return (String) attributes.get("jobTitle");
    }

    public String getDepartment() {
        return (String) attributes.get("department");
    }

    public String getUsername() {
        return (String) attributes.get("username");
    }

    public void setUsername(String username) {
        attributes.put("username", username);
    }

    public String getFirstName() {
        return (String) attributes.get("firstName");
    }

    public String getLastName() {
        return (String) attributes.get("lastName");
    }

    public String getEmail() {
        return (String) attributes.get("email");
    }

    public String getCountry() {
        return (String) attributes.get("country");
    }

    public Sex getSex() {
        return (Sex) attributes.get("sex");
    }

    public void setSex(String sex) {
        if (sex != null && sex.length() > 0) {
            Sex sexEnum = Sex.valueOf(sex);
            attributes.put("sex", sexEnum);
        }
    }

    public Date getDateOfBirth() {
        return (Date) attributes.get("dateOfBirth");
    }

    public void setDateOfBirth(String dateOfBirth) {
        try {
            if (dateOfBirth != null && dateOfBirth.length() > 0) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.US);
                Date dob = simpleDateFormat.parse(dateOfBirth);
                attributes.put("dateOfBirth", dob);
            }
        } catch (ParseException ex) {
            throw new RuntimeException("Invalid date format " + dateOfBirth);
        }
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

    public Map<String, Object> getAttributes() {
        return attributes;
    }

    public void setHostname(String hostname) {
        attributes.put("hostname", hostname);
    }

    public String getHostname() {
        return (String) attributes.get("hostname");
    }

    public void setAttributes(Map<String, Object> attributes) {
        this.attributes = attributes;
    }
}
