/* Copyright 2012 Cloudseal Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cloudseal.client.saml2;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMValidateContext;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.*;
import java.io.*;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.security.PublicKey;
import java.security.Signature;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

/**
 * Central class responsible for validating the SAML Response from the IDP and constructing a CloudsealUser object.
 * It is assumed the IDP will be using the POST binding
 *
 */
public class SamlValidatorImpl implements SamlValidator {

    private static Logger logger = Logger.getLogger(SamlBuilderImpl.class);

    /**
     * <p>Validate the response from the IDP. This method performs several steps as required by the SAML Post binding:</p>
     *
     * <ol>
     *     <li>Base64Decode the response to get the XML message</li>
     *     <li>Verify that the &lt;Assertion&gt; was signed by the IDP</li>
     *     <li>Verify the &lt;Destination&gt; and &lt;NotOnOrAfter&gt;</li>
     *     <li>Extract the principal (username) and attributes</li>
     *     <li>Construct and return a CloudsealUser object</li>
     * </ol>
     *
     * @param idpPublicKey
     * @param encodedResponse
     * @param expectedInResponseTo
     * @return
     * @throws VerificationException
     */
    @Override
    public CloudsealPrincipal validateAuthResponse(PublicKey idpPublicKey, String encodedResponse, String expectedInResponseTo, String expectedAudience) throws VerificationException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setNamespaceAware(true);
        XPathFactory xPathfactory = XPathFactory.newInstance();
        XPath xPath = xPathfactory.newXPath();

        setXPathNamespace(xPath);

        byte[] samlResponseDecoded = Base64.decodeBase64(encodedResponse);
        if (samlResponseDecoded.length < 1) {
            throw new VerificationException("Unable to decode SAML Response");
        }

        String samlResponseDecodedString = new String(samlResponseDecoded, Charset.forName("UTF-8"));
        if (samlResponseDecodedString == null || samlResponseDecoded.length < 1) {
            throw new VerificationException("Unable to reconstruct SAML Response");
        }

        if (logger.isDebugEnabled())  {
            String message = "SAML Response received: " + samlResponseDecodedString;
            logger.debug(message);
        }

        InputSource source = new InputSource(new StringReader(samlResponseDecodedString));
        Document document;
        try {
            document = documentBuilderFactory.newDocumentBuilder().parse(source);
        } catch (Exception ex) {
            throw new VerificationException("Unable to marshall XML", ex);
        }

        verifyResponseSignature(idpPublicKey, document);
        verifyInResponseTo(xPath, document, expectedInResponseTo);
        verifyAudience(xPath, document, expectedAudience);
        return authenticate(xPath, document);
    }

    @Override
    public void validateLogoutRequest(PublicKey idpPublicKey, String queryString) throws VerificationException {
        try {
            Pattern pattern = Pattern.compile("^(SAMLRequest=.*?)&Signature=([^&]+).*");
            Matcher matcher = pattern.matcher(queryString);
            if (! matcher.matches()) {
                throw new VerificationException("Invalid Logout request format according to the HTTP redirect binding format");
            }
            String request = matcher.group(1);
            String signatureText = matcher.group(2);

            byte[] dataToVerify = request.getBytes("UTF-8");
            byte[] signatureBytes = Base64.decodeBase64(URLDecoder.decode(signatureText, "UTF-8"));
            Signature signature = Signature.getInstance("SHA1withRSA");
            signature.initVerify(idpPublicKey);
            signature.update(dataToVerify);
            boolean sigOk = signature.verify(signatureBytes);
            if (! sigOk) {
                throw new VerificationException("Signature does not match public key");
            }
        } catch (Exception ex) {
            throw new VerificationException("Unable to inflate SAML Request", ex);
        }
    }

    protected String inflate(byte[] bytes) throws DataFormatException, UnsupportedEncodingException {
        Inflater decompresser = new Inflater(true);
        decompresser.setInput(bytes, 0, bytes.length);
        byte[] result = new byte[1024];
        int resultLength = decompresser.inflate(result);
        decompresser.end();

        String outputString = new String(result, 0, resultLength, "UTF-8");
        return outputString;
    }

    protected void setXPathNamespace(XPath xPath) {
        NamespaceContext context = new NamespaceContextMap(
                "saml2p", "urn:oasis:names:tc:SAML:2.0:protocol",
                "saml2", "urn:oasis:names:tc:SAML:2.0:assertion"
        );

        xPath.setNamespaceContext(context);
    }

    protected void verifyInResponseTo(XPath xPath, Document document, String expectedInResponseTo) throws VerificationException {
        try {
            XPathExpression expr = xPath.compile("//saml2:SubjectConfirmationData/@InResponseTo");
            String inResponseTo = expr.evaluate(document);
            if (inResponseTo == null || ! inResponseTo.equals(expectedInResponseTo))
                throw new VerificationException("Unable to verify inResponseTo, expected: " + expectedInResponseTo + " actual: " + inResponseTo);
        } catch (XPathExpressionException ex) {
            throw new RuntimeException(ex);
        }
    }

    protected void verifyAudience(XPath xPath, Document document, String expectedAudience) throws VerificationException {
        try {
            XPathExpression expr = xPath.compile("//saml2:AudienceRestriction/saml2:Audience");
            String audience = expr.evaluate(document);
            if (audience == null || ! audience.equals(expectedAudience))
                throw new VerificationException("Unable to verify Audience, expected: " + expectedAudience + " actual: " + audience);
        } catch (XPathExpressionException ex) {
            throw new RuntimeException(ex);
        }
    }

    protected void verifyResponseSignature(PublicKey idpPublicKey, Document document) throws VerificationException {
        // Find Signature element
        NodeList nl = document.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");
        if (nl.getLength() == 0) {
            throw new VerificationException("Cannot find Signature element");
        }

        // Create a DOMValidateContext and specify a KeySelector
        // and document context.
        DOMValidateContext valContext = new DOMValidateContext(new CloudsealKeySelector(idpPublicKey), nl.item(0));

        XMLSignatureFactory factory = XMLSignatureFactory.getInstance("DOM");
        boolean valid = false;
        try {
            XMLSignature signature = factory.unmarshalXMLSignature(valContext);
            valid = signature.validate(valContext);
        } catch (MarshalException ex) {
            throw new VerificationException("Response verification failed");
        } catch (XMLSignatureException ex) {
            throw new VerificationException("Response verification failed");
        }
        if (! valid) {
            throw new VerificationException("Response verification failed");
        }
    }

    protected CloudsealPrincipal authenticate(XPath xPath, Document document) throws VerificationException {
        CloudsealPrincipal cloudsealPrincipal = new CloudsealPrincipal();

        try {
            xPath.reset();
            XPathExpression expression = xPath.compile("/saml2p:Response/saml2:Assertion/saml2:Subject/saml2:NameID");
            String username = expression.evaluate(document);
            if (username == null || username.length() < 1)
                throw  new VerificationException("Unable to retrieve Subject/NameID from SAML response");
            cloudsealPrincipal.setUsername(username);

            xPath.reset();
            String xPathExpressionStr = "//saml2:Attribute";
            expression = xPath.compile(xPathExpressionStr);
            Object results = expression.evaluate(document, XPathConstants.NODESET);
            NodeList nodes = (NodeList) results;
            for (int i=0; i< nodes.getLength(); i++) {
                Element samlAttribute = (Element) nodes.item(i);
                xPathExpressionStr = "@Name";
                expression = xPath.compile(xPathExpressionStr);
                String attributeName = expression.evaluate(samlAttribute);
                // ROLES, Sex and Date of birth need special handling because we want to convert them to typed objects
                if (attributeName != null && attributeName != "ROLES"
                        && attributeName != "sex" && attributeName != "dateOfBirth") {
                    expression = xPath.compile("saml2:AttributeValue");
                    String attributeValue = expression.evaluate(samlAttribute);
                    logger.debug("SAML attribute: " + attributeName + ": " + attributeValue);
                    cloudsealPrincipal.getAttributes().put(attributeName, attributeValue);
                }
            }

            // ROLES, Sex and Date of birth need special handling because we want to convert them to typed objects
            // that's what the setters on CloudsealPrincipal do

            // date of birth
            String dobStr = getAttribute(xPath, document, "birthday");
            cloudsealPrincipal.setDateOfBirth(dobStr);

            // sex
            String sexStr = getAttribute(xPath, document, "sex");
            cloudsealPrincipal.setSex(sexStr);

            // roles
            Set<String> roles = getRoles(xPath, document);
            cloudsealPrincipal.setRoles(roles);


            return cloudsealPrincipal;
        } catch (XPathExpressionException ex) {
            throw new RuntimeException(ex);
        }
    }

    protected String getAttribute(XPath xPath, Document document, String attributeName) throws XPathExpressionException {
        xPath.reset();
        String xPathExpressionStr = String.format(Locale.US, "//saml2:Attribute[@Name='%s']/saml2:AttributeValue", attributeName);
        XPathExpression expression = xPath.compile(xPathExpressionStr);
        String attributeValue = expression.evaluate(document);
        return attributeValue;
    }

    protected Set<String> getRoles(XPath xPath, Document document) throws XPathExpressionException {
        Set<String> roles = new HashSet<String>();

        xPath.reset();
        String xPathExpressionStr = String.format(Locale.US, "//saml2:Attribute[@Name='roles']/saml2:AttributeValue");
        XPathExpression expression = xPath.compile(xPathExpressionStr);
        NodeList nodeList = (NodeList) expression.evaluate(document, XPathConstants.NODESET);
        for (int i=0; i<nodeList.getLength(); i++) {
            Node textNode = nodeList.item(i);
            String roleName = textNode.getTextContent();
            roles.add(roleName);
        }

        return roles;
    }
}
