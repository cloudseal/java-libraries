package com.cloudseal.client.saml2;

/**
 * Created with IntelliJ IDEA.
 * User: toby
 * Date: 11/12/2012
 * Time: 17:27
 * To change this template use File | Settings | File Templates.
 */
public interface SamlBuilder {
    void init(SamlConfig context) throws Exception;
    String generateSamlAuthRequest(String spIssuer, String acsUrl, String destination, String id) throws Exception;
}
