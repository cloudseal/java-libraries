/* Copyright 2012 Cloudseal Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cloudseal.client.saml2;

import static org.junit.Assert.*;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.w3c.dom.Document;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.PublicKey;
import java.util.*;

public class AuthResponseValidatorTests {

    private SamlValidatorImpl classUnderTest;
    private XPath xPath;
    Document doc;

    public AuthResponseValidatorTests() throws Exception {
        classUnderTest = new SamlValidatorImpl();
        XPathFactory xPathfactory = XPathFactory.newInstance();
        xPath = xPathfactory.newXPath();

        NamespaceContext context = new NamespaceContextMap(
                "saml2p", "urn:oasis:names:tc:SAML:2.0:protocol",
                "saml2", "urn:oasis:names:tc:SAML:2.0:assertion"
        );

        xPath.setNamespaceContext(context);

        // Parse the SamlAuthResponse.xml document
        InputStream is = getClass().getClassLoader().getResourceAsStream("SamlAuthResponse.xml");
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setNamespaceAware(true);
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        doc = documentBuilder.parse(is);
        IOUtils.closeQuietly(is);
    }

    @Test
    public void testSignDocument() throws Exception {
        InputStream is = getClass().getClassLoader().getResourceAsStream("SamlAuthResponse.xml");

        // Load the keystore and key the key entry
        is = getClass().getClassLoader().getResourceAsStream("teststore.jks");
        KeyStore keyStore = KeyStore.getInstance("JKS");
        keyStore.load(is, "password".toCharArray());
        IOUtils.closeQuietly(is);
        KeyStore.PrivateKeyEntry keyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry("mykey", new KeyStore.PasswordProtection("password".toCharArray()));

        // Use the document signer class to sign the doc
        DocumentSigner documentSigner = new DocumentSigner();
        // ID maps to the Assertion ID in the xml document
        documentSigner.signRequest(keyEntry, doc, "_7b42b98bb16771e98c982b77e7288bd3");

        // get the public key from the keyentry
        PublicKey publicKey = keyEntry.getCertificate().getPublicKey();

        classUnderTest.verifyResponseSignature(publicKey, doc);
    }

    @Test
    public void testSignDocumentWithWrongKey() throws Exception {
        InputStream is = getClass().getClassLoader().getResourceAsStream("SamlAuthResponse.xml");

        // Load the keystore and key the key entry
        is = getClass().getClassLoader().getResourceAsStream("teststore.jks");
        KeyStore keyStore = KeyStore.getInstance("JKS");
        keyStore.load(is, "password".toCharArray());
        IOUtils.closeQuietly(is);
        KeyStore.PrivateKeyEntry hackersKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry("wrongkey", new KeyStore.PasswordProtection("password".toCharArray()));

        // Use the document signer class to sign the doc with the hackers key
        DocumentSigner documentSigner = new DocumentSigner();
        // ID maps to the Assertion ID in the xml document
        documentSigner.signRequest(hackersKeyEntry, doc, "_7b42b98bb16771e98c982b77e7288bd3");

        // This is the key we expect the document to be signed with
        KeyStore.PrivateKeyEntry correctKeyEntry = (KeyStore.PrivateKeyEntry) keyStore.getEntry("mykey", new KeyStore.PasswordProtection("password".toCharArray()));
        // get the public key from the keyentry
        PublicKey publicKey = correctKeyEntry.getCertificate().getPublicKey();

        try {
            classUnderTest.verifyResponseSignature(publicKey, doc);
            fail("Expected an exception");
        } catch (VerificationException ex) {
            assertEquals("Response verification failed", ex.getMessage());
        }
    }

    @Test
    public void testVerifyInResponseTo() throws Exception {
        // Correct response
        classUnderTest.verifyInResponseTo(xPath, doc, "_b9d5550c9fb23d96c68ab868d5d91fa3");
        // Man in middle attack
        try {
            classUnderTest.verifyInResponseTo(xPath, doc, "123");
            fail("Expected error");
        } catch (VerificationException ex) {
            assertEquals("Unable to verify inResponseTo, expected: 123 actual: _b9d5550c9fb23d96c68ab868d5d91fa3", ex.getMessage());
        }
    }

    @Test
    public void testAuthenticate() throws Exception {
        CloudsealPrincipal cloudsealPrincipal = classUnderTest.authenticate(xPath, doc);
        assertEquals("thobson", cloudsealPrincipal.getUsername());
        assertEquals("Toby", cloudsealPrincipal.getFirstName());
        assertEquals("Hobson", cloudsealPrincipal.getLastName());
        assertEquals("toby.hobson@cloudseal.com", cloudsealPrincipal.getEmail());
        assertEquals("United Kingdom", cloudsealPrincipal.getCountry());
        assertEquals("12345", cloudsealPrincipal.getPhone());
        assertEquals("SW1X 1XX", cloudsealPrincipal.getPostCode());
        assertEquals("An Address\nIn London", cloudsealPrincipal.getAddress());
        assertEquals("Cloudseal", cloudsealPrincipal.getCompany());
        assertEquals("IT", cloudsealPrincipal.getDepartment());
        assertEquals("CTO", cloudsealPrincipal.getJobTitle());

        assertEquals(Sex.MALE, cloudsealPrincipal.getSex());
        Date birthDate = cloudsealPrincipal.getDateOfBirth();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(birthDate);
        assertEquals(1, calendar.get(Calendar.DAY_OF_MONTH));
        assertEquals(Calendar.NOVEMBER, calendar.get(Calendar.MONTH));
        assertEquals(2012, calendar.get(Calendar.YEAR));

        Set<String> actualRoles = cloudsealPrincipal.getRoles();
        assertTrue(actualRoles.contains("ROLE_USER"));
        assertTrue(actualRoles.contains("ROLE_ADMIN"));
        assertEquals(2, actualRoles.size());
    }

    @Test
    public void testWrongAudience() throws Exception {
        try {
            classUnderTest.verifyAudience(xPath, doc, "http://somethingelse.com/saml/sp");
            fail("expected error");
        } catch(VerificationException ex) {
            assertEquals("Unable to verify Audience, expected: http://somethingelse.com/saml/sp actual: http://localhost:8081/saml/sp", ex.getMessage());
        }
    }

}
