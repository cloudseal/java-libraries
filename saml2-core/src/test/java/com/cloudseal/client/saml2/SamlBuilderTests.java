/* Copyright 2012 Cloudseal Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cloudseal.client.saml2;

import static org.junit.Assert.*;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

public class SamlBuilderTests extends SamlBuilderImpl {

    public static final String SAMLP_NAMESPACE = "urn:oasis:names:tc:SAML:2.0:protocol";
    private SamlBuilderImpl classUnderTest;

    @Before
    public void setup() throws Exception {
        classUnderTest = new SamlBuilderImpl();
        InputStream is = getClass().getClassLoader().getResourceAsStream("teststore.jks");
        byte[] keystore = IOUtils.toByteArray(is);
        IOUtils.closeQuietly(is);
        SamlConfig context = new SamlConfig();
        context.setKeystore(keystore);
        context.setKeyName("mykey");
        context.setKeystorePassword("password");
        context.setKeyPassword("password");
        classUnderTest.init(context);
    }

    @Test
    public void testGenerateIssueDate() throws ParseException {
        String dateString = classUnderTest.generateIssueDate();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
        Date now = format.parse(dateString);
        assertNotNull(now);
    }

    /**
     * Quick sanity test to ensure the code is able to serialize a com.cloudseal.client.spring.namespace aware document.
     *
     * @throws Exception
     */
    @Test
    public void testMarshallDocument() throws Exception {
        // Load the correct XML and create a document
        InputStream is = getClass().getClassLoader().getResourceAsStream("SamlAuthRequest.xml");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);

        Document doc =  factory
                .newDocumentBuilder()
                .parse(is);
        IOUtils.closeQuietly(is);

        // Serialize the doc using the AuthRequestBuilder
        String serializedXml = classUnderTest.marshallDocument(doc);

        // Create a new doc based on the serialized xml
        Document doc2 =  factory
                .newDocumentBuilder()
                .parse(new ByteArrayInputStream(serializedXml.getBytes()));

        // perform some assertions on this new doc
        Element rootTag = (Element) doc2.getElementsByTagNameNS(SAMLP_NAMESPACE, "AuthnRequest").item(0);
        assertNotNull(rootTag);
        String acsUrl = rootTag.getAttribute("AssertionConsumerServiceURL");
        assertEquals("http://localhost:8081/test/acs", acsUrl);
        String destination = rootTag.getAttribute("Destination");
        assertEquals("http://localhost:8081/test/acs", destination);
        String ID = rootTag.getAttribute("ID");
        assertEquals("_9e6f56ec8ebd4eaf703da90dc0853edd", ID);
    }

    @Test
    public void testBuildAuthRequest() throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);

        Document doc = factory.newDocumentBuilder().newDocument();
        String acsUrl = "http://www.cloudseal.com/test/acs";
        String destination = "http://www.cloudseal.com/test/acs";
        String id = "12345";
        String expectedSpIssuer = "http://www.cloudseal.com/test/saml/auth";

        Element root = (Element) classUnderTest.buildAuthRequest(doc, acsUrl, destination, id, expectedSpIssuer, null);
        assertEquals("samlp:AuthnRequest", root.getTagName());
        assertEquals(SAMLP_NAMESPACE, root.getNamespaceURI());
        assertNotNull(root.getAttribute("ID"));
        assertNotNull(root.getAttribute("IssueInstant"));
        assertNotNull(root.getAttribute("AssertionConsumerServiceURL"));
        assertEquals(acsUrl, root.getAttribute("AssertionConsumerServiceURL"));
        assertEquals(destination, root.getAttribute("Destination"));
        assertEquals("false", root.getAttribute("IsPassive"));
        assertEquals("urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST", root.getAttribute("ProtocolBinding"));
        assertEquals("", root.getAttribute("ProviderName"));
        assertEquals("2.0", root.getAttribute("Version"));
        String spIssuer = ((Element) root.getElementsByTagNameNS("urn:oasis:names:tc:SAML:2.0:assertion", "Issuer").item(0)).getTextContent();
        assertEquals(expectedSpIssuer, spIssuer);

        // Now set appId (provider name)
        Document doc2 = factory.newDocumentBuilder().newDocument();
        String appId = "testApp";
        Element root2 = (Element) classUnderTest.buildAuthRequest(doc2, acsUrl, destination, id, expectedSpIssuer, appId);
        assertEquals(appId, root2.getAttribute("ProviderName"));
    }

    @Test
    public void testDeflate() throws Exception {
        InputStream is = getClass().getClassLoader().getResourceAsStream("SamlAuthRequest.xml");
        String xml = IOUtils.toString(is);
        IOUtils.closeQuietly(is);

        byte[] deflatedData = classUnderTest.deflate(xml);

        Inflater decompresser = new Inflater(true);
        decompresser.setInput(deflatedData, 0, deflatedData.length);
        byte[] result = new byte[1024];
        decompresser.inflate(result);
        decompresser.end();
        // Need to trim the whitespace for primative string based equality tests
        assertEquals(xml.trim(), new String(result).trim());
    }

    @Test
    public void testBase64Encode() throws Exception {
        // Simple test
        String expected = "aGVsbG8gd29ybGQ=";
        String input = "hello world";
        String actual = classUnderTest.base64Encode(input.getBytes("UTF-8"));
        assertEquals(expected, actual);

        // Test with deflated data
        InputStream is = getClass().getClassLoader().getResourceAsStream("SamlAuthRequest.xml");
        String xml = IOUtils.toString(is);
        IOUtils.closeQuietly(is);

        // Simulate deflating the xml
        byte[] buffer = new byte[1024];
        Deflater compresser = new Deflater(Deflater.DEFAULT_COMPRESSION, true);
        compresser.setInput(xml.getBytes("UTF-8"));
        compresser.finish();
        int compressedDataLength = compresser.deflate(buffer);
        byte[] deflatedData = new byte[compressedDataLength];
        for (int i=0; i<compressedDataLength; i++) {
            deflatedData[i] = buffer[i];
        }
        compresser.reset();

        String base64EncodedText = classUnderTest.base64Encode(deflatedData);

        InputStream is2 = getClass().getClassLoader().getResourceAsStream("Base64DeflatedAndEncodedXml.text");
        String expectedText = IOUtils.toString(is2);
        IOUtils.closeQuietly(is);

        assertEquals(expectedText, base64EncodedText);
    }

}
