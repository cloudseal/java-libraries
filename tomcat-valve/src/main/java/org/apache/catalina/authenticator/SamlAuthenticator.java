package org.apache.catalina.authenticator;

import com.cloudseal.client.saml2.*;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.Session;
import org.apache.catalina.connector.Request;
import org.apache.catalina.connector.Response;
import org.apache.catalina.realm.GenericPrincipal;
import org.apache.commons.io.IOUtils;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

public class SamlAuthenticator extends FormAuthenticator {

    private static final Log LOG = LogFactory.getLog(SamlAuthenticator.class);

    // If the developer specifies an auth-method in web.xml it must be set to this value
    private static final String AUTH_METHOD = "SAML2";

    // path to which SAML responses will be sen
    private static final String ACS_PATH = "/saml_acs";

    // If a user sends a request to this url it will redirect the user to the IDP's logout url
    private static final String SLO_REQUEST_URL = "/slo_logout";

    // Note: Cloudseal specific (optional), The Cloudseal platform will trigger a request from the user's browser
    // to this url and the valve will intercept it, perform a logout and then return a json message
    // see https://cloudseal.jira.com/wiki/display/WIKI/Cloudseal+logout+protocol
    private static final String BROWSER_INITIATED_SLO_URL = "/logout.jsonp";

    // A session key for the SAML request id
    private static final String ID = "org.apache.catalina.authenticator.SamlRequestId";

    // A session key for the SAML audience
    private static final String AUDIENCE = "org.apache.catalina.authenticator.SamlAudience";

    // Generates a random string
    private IdentifierGenerator idGenerator;

    // Builds the authentication request
    private SamlBuilder samlBuilder;

    // Validates the authentication response from the IDP
    private SamlValidator samlValidator;

    // Parses the idp.xml file and extracts the urls and the public key that we
    // will use to verify the response from the IDP is authentic
    private IdpXmlParser idpXmlParser;

    // location of idp.xml
    private String idp;

    // location of the keystore
    private String keystore;

    // keystore password
    private String keystorePassword;

    // keystore key (alias) name
    private String key;

    // key password
    private String keyPassword;

    // A calling application id which will be mapped to the ProviderName element in the SAMLRequest (optional)
    private String appId;

    private String sloUrl;

    public SamlAuthenticator() throws Exception {
        setAlwaysUseSession(true);
    }

    @Override
    protected String getAuthMethod() {
        return AUTH_METHOD;
    }

    @Override
    protected synchronized void initInternal() throws LifecycleException {
        super.initInternal();
        LOG.info("Initializing");
        // Validate that all required properties are set
        validateConfigParam(idp, "idp");
        validateConfigParam(keystore, "keystore");
        validateConfigParam(keystorePassword, "keystorePassword");
        validateConfigParam(key, "key");
        validateConfigParam(keyPassword, "keyPassword");

        try {
            // AuthRequestContext is simply a Javabean that allows us to pass data between the various
            // SAML collaborators
            SamlConfig samlConfig = new SamlConfig();
            samlConfig.setKeyName(key);
            samlConfig.setKeyPassword(keyPassword);
            samlConfig.setKeystore(getResource(keystore));
            samlConfig.setKeystorePassword(keystorePassword);
            samlConfig.setProviderName(appId);

            // Create and initialize the SAML objects, the null tests are there to aid unit testing
            // as we also have package visible setters for these objects (which could be mocks)
            if (samlBuilder == null)
                samlBuilder = new SamlBuilderImpl();

            samlBuilder.init(samlConfig);

            if (samlValidator == null)
                samlValidator = new SamlValidatorImpl();

            if (idpXmlParser == null)
                idpXmlParser = new IdpXmlParser();

            idpXmlParser.parse(getResource(idp));
            sloUrl = idpXmlParser.getSloUrl();

            if (idGenerator == null)
                idGenerator = new IdentifierGenerator();
        } catch (Exception e) {
            throw new LifecycleException("Failed to initialize", e);
        }
    }

    /**
     * Adds a check to detect if the browser issued a jsonp logout request. Note: this is currently Cloudseal
     * specific as we are not aware of any other IDPs which implement this. However it will have no impact
     * when used with other IDPs, as it will ignore other requests and pass them through to the next valve in the
     * pipeline.
     *
     * @param request Request to be processed
     * @param response Response to be processed
     *
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void invoke(Request request, Response response) throws IOException, ServletException {
        LOG.info("invoke()");

        if (request.getRequestURL().toString().endsWith(SLO_REQUEST_URL)) {
            handleSloRequest(request, response);
            return;
        }

        // single log out request from IDP? Note: this is Cloudseal specific but wont have any impact on other IDPs because they
        // wont issue such requests
        if (request.getRequestURL().toString().endsWith(BROWSER_INITIATED_SLO_URL)) {
            handleBrowserInitiatedSlo(request, response);
            return;
        }

        // SAMLResponse will be sent to an unsecured URL so we need to catch it here and process it
        String contextPath = this.context.getPath();
        String requestURI = request.getDecodedRequestURI();
        if (requestURI.startsWith(contextPath) &&
                requestURI.endsWith(ACS_PATH)) {
            if (handleSamlResponse(request, response, requestURI))
                return;
        }

        if (context.getLoginConfig() != null) {
            String authMethod = context.getLoginConfig().getAuthMethod();
            // Without registering the authenticator and its auth method in the container,
            // it won't even invoke the valve if unknown auth method encountered in app's web.xml.
            // If login-config is omitted then by default auth method is NONE.
            if (AUTH_METHOD.equals(authMethod) || "NONE".equals(authMethod)) {
                super.invoke(request, response);
                return;
            }
        }

        getNext().invoke(request, response);
    }

    private void handleBrowserInitiatedSlo(Request request, Response response) throws ServletException, IOException {
        LOG.info("browser initiated single log out");
        try {
            samlValidator.validateLogoutRequest(idpXmlParser.getIdpPublicKey(), request.getQueryString());
        } catch (VerificationException ex) {
            LOG.error("Unable to verify authenticity of logout request", ex);
            throw new ServletException("Unable to verify authenticity of logout request");
        }

        logout(request);
        // Send the jsonp response back to Cloudseal
        String callback = request.getParameter("callback");
        String jsonp = String.format("%s({\"status\":\"success\"})", callback);
        response.getWriter().print(jsonp);
        response.getWriter().flush();
        return;
    }

    private boolean handleSamlResponse(Request request, Response response, String requestURI) throws IOException {
        if (request.getSessionInternal().getPrincipal() != null) {
            // The user is trying to resubmit the SAML response from the IDP even though he's already authenticated
            // The most likely scenario is that he the back button immediately after successfully authenticating.
            // If this happens we would get an error because the SAML response validator will try to find the
            // original SAML request in the session to match the ID and Audience but it wont exist
            // (we clear the session on successful authenitcation). a nasty solution is to write some javascript to
            // the browser telling it to go back again (which is what the user really wanted to do). However it may
            // be safer to just display a message telling the user what's happened
            StringBuilder htmlResponse = new StringBuilder();
            htmlResponse.append("<!DOCTYPE html>");
            htmlResponse.append("<html>");
            htmlResponse.append("<body>");
            htmlResponse.append("<p>You have most likely reached this page because you used the browser's ");
            htmlResponse.append("back button immediately after authenticating. Please use the navigation ");
            htmlResponse.append("links provided</p>");
            htmlResponse.append("</body>");
            htmlResponse.append("</html>");
            response.getWriter().write(htmlResponse.toString());
            return true;
        }
        if (!authenticate(request, response)) {
            if (LOG.isDebugEnabled())
                LOG.debug(" Failed authenticate() test ??" + requestURI );
            return true;
        }
        return false;
    }

    private void handleSloRequest(Request request, Response response) throws ServletException, IOException {
        String sloUrl = idpXmlParser.getSloUrl();
        response.sendRedirect(sloUrl);
    }

    /**
     * Authenticate the user making this request, based on the specified
     * login configuration.  Return <code>true</code> if any specified
     * constraint has been satisfied, or <code>false</code> if we have
     * created a response challenge already.
     *
     * Note: most of the code here is lifted from the FormAuthenticator
     * so there is scope to refactor
     *
     * @param request   Request we are processing
     * @param response  Response we are creating
     *
     * @exception IOException if an input/output error occurs
     */
    @Override
    public boolean authenticate(Request request,
                                HttpServletResponse response)
            throws IOException {

        // References to objects we will need later
        Session session = null;

        // Have we already authenticated someone?
        Principal principal = request.getUserPrincipal();
        String ssoId = (String) request.getNote(Constants.REQ_SSOID_NOTE);
        if (principal != null) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Already authenticated '" +
                        principal.getName() + "'");
            }
            // Associate the session with any existing SSO session
            if (ssoId != null) {
                associate(ssoId, request.getSessionInternal(true));
            }
            return (true);
        }  // if principal != null

        // Is there an SSO session against which we can try to reauthenticate?
        if (ssoId != null) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("SSO Id " + ssoId + " set; attempting " +
                        "reauthentication");
            }
            // Try to reauthenticate using data cached by SSO.  If this fails,
            // either the original SSO logon was of DIGEST or SSL (which
            // we can't reauthenticate ourselves because there is no
            // cached username and password), or the realm denied
            // the user's reauthentication for some reason.
            // In either case we have to prompt the user for a logon */
            if (reauthenticateFromSSO(ssoId, request)) {
                return true;
            }
        } // id ssoId != null

        // Is this the re-submit of the original request URI after successful
        // authentication?  If so, forward the *original* request instead.
        if (matchRequest(request)) {
            session = request.getSessionInternal(true);
            if (LOG.isDebugEnabled()) {
                LOG.debug("Restore request from session '"
                        + session.getIdInternal()
                        + "'");
            }

            // CloudsealPrincipal (from com.cloudseal.client.saml2-core) includes additional fields
            // e.g. firstName, lastName etc which may be passed from the IDP. Not all IDPs will support all fields
            // however the code is null safe, the only requirement is that the IDP returns a username
            CloudsealPrincipal cloudsealPrincipal = (CloudsealPrincipal)
                    session.getNote(Constants.FORM_PRINCIPAL_NOTE);

            // Some IDP's may manage authorities (roles) along with identity
            // and the authenticator should be able to use these
            if (cloudsealPrincipal.getRoles() != null && ! cloudsealPrincipal.getRoles().isEmpty()) {
                // Create a new GenericPrincipal based on the CloudsealPrincipal
                // (Including the roles returned by the IDP)
                List<String> roles = new ArrayList<String>(cloudsealPrincipal.getRoles());
                principal = new GenericPrincipal(cloudsealPrincipal.getUsername(), null, roles, cloudsealPrincipal);
            } else {
                // Just return the CloudsealPrincipal without any roles
                principal = cloudsealPrincipal;
            }

            // authenticate and save to session (this will create new session)
            register(request, response, principal, getAuthMethod(), principal.getName(), null);

            if (restoreRequest(request, session)) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Proceed to restored request");
                }
                return (true);
            } else {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Restore of original request failed");
                }
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                return (false);
            }
        }  // if matchRequest

        // is this a response from the IDP
        boolean samlResponse = request.getRequestURI().endsWith(ACS_PATH)
                && request.getParameter("SAMLResponse") != null;

        // No -- Save this request and redirect to the IDP
        if (! samlResponse) {
            session = request.getSessionInternal(true);
            if (LOG.isDebugEnabled()) {
                LOG.debug("Save request in session '" + session.getIdInternal() + "'");
            }
            try {
                saveRequest(request, session);
            } catch (IOException ioe) {
                LOG.debug("Request body too big to save during authentication");
                response.sendError(HttpServletResponse.SC_FORBIDDEN, "Response body too big");
                return (false);
            }
            redirectToIdp(request, response);
            return (false);
        } // if !samlResponse

        // Yes -- validate the specified credentials
        // and redirect to the error page if they are not correct
        session = request.getSessionInternal(false);
        if (session == null) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("User took so long to log on the session expired");
            }
            response.sendError(HttpServletResponse.SC_REQUEST_TIMEOUT, "Session expired");
            return (false);
        }

        // SAML2 concepts, when we redirect the user to the IDP we pass these attributes
        // and we expect the response to match
        String expectedInResponseTo = (String) session.getSession().getAttribute(ID);
        String expectedAudience = (String) session.getSession().getAttribute(AUDIENCE);
        try {
            principal = samlValidator.validateAuthResponse(idpXmlParser.getIdpPublicKey(),
                    request.getParameter("SAMLResponse"), expectedInResponseTo, expectedAudience);
            LOG.debug("Authentication of '" + principal.getName() + "' was successful");
            session.getSession().removeAttribute(ID);
            session.getSession().removeAttribute(AUDIENCE);
        } catch (VerificationException ex) {
            LOG.error("SAML verification failed: ", ex);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Unable to validate SAML response");
            return (false);
        }

        // Save the authenticated Principal in our session
        // Note we need to use Constants.FORM_PRICIPAL_NOTE so we can reuse the matchRequest()
        // method in FormAuthenticator
        session.setNote(Constants.FORM_PRINCIPAL_NOTE, principal);

        // Redirect the user to the original request URI (which will cause
        // the original request to be restored)
        String requestURI = savedRequestURL(session);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Redirecting to original '" + requestURI + "'");
        }
        if (requestURI == null) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "SAML authenticator");
        } else {
            response.sendRedirect(response.encodeRedirectURL(requestURI));
        }
        return (false);
    }

    /**
     * Send an authentication request to the IDP using the SAML redirect binding. Other bindings could be supported
     * with relative ease.
     *
     * @param request
     * @param response
     * @throws IOException
     */
    void redirectToIdp(Request request, HttpServletResponse response) throws IOException {
        try {
            String spIssuer = buildSpIssuer(request);
            String id = idGenerator.generateIdentifier();
            String acsUrl = buildAcsUrl(request);
            request.getSession().setAttribute(ID, id);
            request.getSession().setAttribute(AUDIENCE, spIssuer);
            String samlAuthRequest = samlBuilder.generateSamlAuthRequest(spIssuer, acsUrl, spIssuer, id);
            String redirectUrl = idpXmlParser.getSsoUrl() + "?" + samlAuthRequest;
            response.sendRedirect(redirectUrl);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            sendForbiddenError(response, e.getMessage());
        }
    }

    /**
     * Attempt to locate a resource. By default it will look for the named resource using the ServletContext, however
     * if the classpath: prefix is used it will look for the resource on the classpath (which will be $CATALINA_HOME/lib
     * as this is where this class will reside)
     *
     * @param filename
     * @return
     * @throws IOException
     */
    byte[] getResource(String filename) throws IOException {
        ServletContext servletContext = this.context.getServletContext();
        InputStream is;
        if (filename.startsWith("classpath:")) {
            String path = filename.split(":")[1];
            is = this.getClass().getClassLoader().getResourceAsStream(path);
        } else {
            is = servletContext.getResource(filename).openStream();
        }
        try {
            return IOUtils.toByteArray(is);
        } finally {
            IOUtils.closeQuietly(is);
        }
    }

    /**
     * All SAML2 requests need an Issuer element, we use the hostname entered by the user
     *
     * @param request
     * @return
     */
    String buildSpIssuer(Request request) {
        String host = request.getHeader("Host");
        String protocol = "http://";
        if (host != null && host.length() > 0) {
            return protocol + host + "/saml/sp";
        } else {
            return protocol + "www.mycorp.com" + "/saml/sp";
        }
    }

    /**
     * Work out where to send the IDP response to. For convenience we use the current url because we can be sure
     * it will hit this valve. Alternatively we could use a hard coded value such as /acs but the url would have to
     * be absolute as this url will be used by the IDP (which is most likely running on another host)
     *
     * @param request
     * @return
     */
    String buildAcsUrl(Request request) {
        String scheme = request.getScheme();
        String host = request.getHeader("host");
        String path = request.getContextPath();
        StringBuilder builder =  new StringBuilder();
        builder.append(scheme);
        builder.append("://");
        builder.append(host);
        builder.append(path);
        builder.append(ACS_PATH);
        return builder.toString();
    }

    /**
     * Simple utility method which verifies that the specified parameter is not null and throws an exception if it is
     *
     * @param param
     * @param paramName
     * @throws LifecycleException
     */
    void validateConfigParam(String param, String paramName) throws LifecycleException {
        if (param == null || param.length() < 1) {
            throw new LifecycleException("Missing configuration parameter: " + paramName);
        }
    }

    /**
     * Sends a 403 error
     *
     * @param response
     * @param error
     * @throws IOException
     */
    void sendForbiddenError(HttpServletResponse response, String error) throws IOException {
        response.sendError(HttpServletResponse.SC_FORBIDDEN, error);
    }

    /**
     * Location of the idp.xml file relative to the ServletContext e.g /WEB-INF/idp.xml
     *
     * @param idp
     */
    public void setIdp(String idp) {
        this.idp = idp;
    }

    /**
     * Location of the keystore relative to the ServletContext e.g /WEB-INF/keystore.jks
     * @param keystore
     */
    public void setKeystore(String keystore) {
        this.keystore = keystore;
    }

    /**
     * Keystore password
     *
     * @param keystorePassword
     */
    public void setKeystorePassword(String keystorePassword) {
        this.keystorePassword = keystorePassword;
    }

    /**
     * Keystore key (alias) name, used to sign authentication requests
     *
     * @param key
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Password for the key
     *
     * @param keyPassword
     */
    public void setKeyPassword(String keyPassword) {
        this.keyPassword = keyPassword;
    }

    /**
     * Allows the authenticator to pass some form of calling application id to the IDP in the SAMLRequest
     * it will be mapped to the ProviderName element in the SAMLRequest.
     *
     * @param appId
     */
    public void setAppId(String appId) {
        this.appId = appId;
    }

    /*
     * Allows for easier unit testing
     */
    void setIdGenerator(IdentifierGenerator idGenerator) {
        this.idGenerator = idGenerator;
    }

    void setSamlBuilder(SamlBuilder samlBuilder) {
        this.samlBuilder = samlBuilder;
    }

    void setSamlValidator(SamlValidator samlValidator) {
        this.samlValidator = samlValidator;
    }

    void setIdpXmlParser(IdpXmlParser idpXmlParser) {
        this.idpXmlParser = idpXmlParser;
    }

}
