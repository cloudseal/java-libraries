/* Copyright 2012 Cloudseal Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cloudseal.client.spring;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@RunWith(MockitoJUnitRunner.class)
public class CloudsealAuthenticationFilterTest {

    private CloudsealAuthenticationFilter classUnderTest;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private HttpSession session;
    @Mock
    private AuthenticationManager authenticationManager;

    @Before
    public void setup() {
        classUnderTest = new CloudsealAuthenticationFilter();
        classUnderTest.setAuthenticationManager(authenticationManager);

    }

    @Test
    public void testSuccessfulAuthentication() throws IOException, ServletException {
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute(CloudsealEntryPoint.AUTH_REQUEST_ID)).thenReturn("123");
        when(session.getAttribute(CloudsealEntryPoint.AUDIENCE)).thenReturn("http://localhost:8080/saml/sp");
        when(request.getParameter("SAMLResponse")).thenReturn("SAMLResponse");
        classUnderTest.attemptAuthentication(request, response);
        verify(authenticationManager).authenticate(argThat(new AuthTokenMatcher()));
    }

    @Test
    public void testUnsuccessfulAuthentication() throws IOException, ServletException {
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute(CloudsealEntryPoint.AUTH_REQUEST_ID)).thenReturn("123");
        when(session.getAttribute(CloudsealEntryPoint.AUDIENCE)).thenReturn("http://localhost:8080/saml/sp");
        when(request.getParameter("SAMLResponse")).thenReturn("SAMLResponse");
        when(authenticationManager.authenticate(any(Authentication.class))).thenThrow(new BadCredentialsException("oops"));
        try {
            classUnderTest.attemptAuthentication(request, response);
            fail("expected error");
        } catch (AuthenticationException ex) {
            assertTrue(BadCredentialsException.class.isAssignableFrom(ex.getClass()));
        }
        verify(authenticationManager).authenticate(argThat(new AuthTokenMatcher()));
    }

    class AuthTokenMatcher extends ArgumentMatcher<CloudsealAssertionAuthenticationToken> {

        @Override
        public boolean matches(Object o) {
            CloudsealAssertionAuthenticationToken token = (CloudsealAssertionAuthenticationToken) o;
            CloudsealAssertionAuthenticationToken.TokenDetails tokenDetails = (CloudsealAssertionAuthenticationToken.TokenDetails) token.getDetails();
            assertEquals("http://localhost:8080/saml/sp", tokenDetails.getAudience());
            assertEquals("123", tokenDetails.getRequestId());
            assertEquals("SAMLResponse", tokenDetails.getSamlResponse());
            return true;
        }
    }
}
