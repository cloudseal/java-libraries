/* Copyright 2012 Cloudseal Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cloudseal.client.spring;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import com.cloudseal.client.saml2.IdentifierGenerator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@RunWith(MockitoJUnitRunner.class)
public class CloudsealEntryPointTest {

    private CloudsealEntryPoint classUnderTest;

    @Mock
    private IdentifierGenerator identifierGenerator;
    @Mock
    private CloudsealManager cloudsealManager;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    @Mock
    private HttpSession session;

    @Before
    public void setup() {
        classUnderTest = new CloudsealEntryPoint(identifierGenerator);
        classUnderTest.setCloudsealManager(cloudsealManager);
    }

    @Test
    public void testBuildAcsUrl() {
        // typical
        testAcsUrl(false, "www.mycorp.com", "", "http://www.mycorp.com/cloudseal_acs");
        testAcsUrl(true, "www.mycorp.com", "", "https://www.mycorp.com/cloudseal_acs");

        // non-standard port
        testAcsUrl(false, "www.mycorp.com:8080", "", "http://www.mycorp.com:8080/cloudseal_acs");
        testAcsUrl(true, "www.mycorp.com:8080", "", "https://www.mycorp.com:8080/cloudseal_acs");

        // non-root context
        testAcsUrl(false, "www.mycorp.com", "/testApp", "http://www.mycorp.com/testApp/cloudseal_acs");
    }

    @Test
    public void testSpIssuer() {
        testSpIssuer("www.mycorp.com", "http://www.mycorp.com/saml/sp");
        testSpIssuer(null, "http://www.mycompany.com/saml/sp");
        testSpIssuer("", "http://www.mycompany.com/saml/sp");
        testSpIssuer("www.mycorp.com:8080", "http://www.mycorp.com:8080/saml/sp");
    }

    @Test
    public void testAfterPropertiesSet() {
        try {
            classUnderTest.afterPropertiesSet();
        } catch (Exception ex) {
            fail("cloudsealManager was set so this should pass");
        }

        try {
            classUnderTest.setCloudsealManager(null);
            classUnderTest.afterPropertiesSet();
            fail("cloudsealManager was not set so this should fail");
        } catch (Exception ex) {
            assertEquals("cloudsealManager must be specified", ex.getMessage());
        }
    }

    @Test
    public void testCommence() throws Exception {
        String id = "123";
        when(identifierGenerator.generateIdentifier()).thenReturn(id);
        when(request.isSecure()).thenReturn(false);
        when(request.getHeader("Host")).thenReturn("www.mycorp.com");
        when(request.getContextPath()).thenReturn("");
        when(cloudsealManager.generateSamlAuthRequest("http://www.mycorp.com/saml/sp",
                                                 "http://www.mycorp.com/cloudseal_acs",
                                                 "http://www.mycorp.com/cloudseal_acs", id)).thenReturn("SAMLRequest=XXX");

        when(cloudsealManager.getSsoUrl()).thenReturn("http://demo.cloudseal.com/idp/saml");
        when(request.getSession()).thenReturn(session);

        ArgumentCaptor<String> redirectUrlCaptor = ArgumentCaptor.forClass(String.class);

        classUnderTest.commence(request, response, new AuthenticationCredentialsNotFoundException("must authenticate"));

        verify(cloudsealManager).generateSamlAuthRequest("http://www.mycorp.com/saml/sp",
                                                    "http://www.mycorp.com/cloudseal_acs",
                                                    "http://www.mycorp.com/cloudseal_acs", id);
        verify(session).setAttribute(CloudsealEntryPoint.AUTH_REQUEST_ID, id);
        verify(response).sendRedirect(redirectUrlCaptor.capture());
        String expectedRedirectUrl = "http://demo.cloudseal.com/idp/saml?SAMLRequest=XXX";
        String actualRedirectUrl = redirectUrlCaptor.getValue();
        assertEquals(expectedRedirectUrl, actualRedirectUrl);
    }

    private void testAcsUrl(boolean isSecure, String host, String contextPath, String expected) {
        reset(request);
        when(request.isSecure()).thenReturn(isSecure);
        when(request.getHeader("Host")).thenReturn(host);
        when(request.getContextPath()).thenReturn(contextPath);
        String actual = classUnderTest.buildAcsUrl(request);
        assertEquals(expected, actual);
    }

    private void testSpIssuer(String host, String expected) {
        reset(request);
        when(request.getHeader("Host")).thenReturn(host);
        String actual = classUnderTest.buildSpIssuer(request);
        assertEquals(expected, actual);
    }
}
