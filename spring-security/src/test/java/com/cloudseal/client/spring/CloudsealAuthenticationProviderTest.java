/* Copyright 2012 Cloudseal Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cloudseal.client.spring;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import com.cloudseal.client.saml2.CloudsealPrincipal;
import com.cloudseal.client.saml2.SamlValidatorImpl;
import com.cloudseal.client.saml2.VerificationException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;

import java.security.PublicKey;
import java.util.Collection;
import java.util.Iterator;

@RunWith(MockitoJUnitRunner.class)
public class CloudsealAuthenticationProviderTest {

    private CloudsealAuthenticationProvider classUnderTest;

    @Mock
    private CloudsealManager cloudsealManager;

    @Mock
    private SamlValidatorImpl authResponseValidator;

    @Mock
    private PublicKey publicKey;

    @Before
    public void setup() {
        classUnderTest = new CloudsealAuthenticationProvider();
    }

    @Test
    public void testSuccessfulAuthentication() throws VerificationException {
        CloudsealPrincipal cloudsealPrincipal = new CloudsealPrincipal();
        cloudsealPrincipal.setUsername("jdoe");
        CloudsealAssertionAuthenticationToken token = new CloudsealAssertionAuthenticationToken("http://localhost:8080/saml/sp", "123", "SAMLResponse");
        when(cloudsealManager.getPublicKey()).thenReturn(publicKey);
        when(authResponseValidator.validateAuthResponse(any(PublicKey.class), anyString(), anyString(), anyString())).thenReturn(cloudsealPrincipal);
        classUnderTest.setCloudsealManager(cloudsealManager);
        classUnderTest.setResponseValidator(authResponseValidator);

        CloudsealAuthenticationToken authToken = (CloudsealAuthenticationToken) classUnderTest.authenticate(token);
        CloudsealUserDetails userDetails = (CloudsealUserDetails) authToken.getPrincipal();
        assertEquals("jdoe", userDetails.getUsername());
        assertTrue(userDetails.getAuthorities().isEmpty());
        assertTrue(authToken.isAuthenticated());
    }

    @Test
    public void testSuccessfulAuthenticationWithRoles() throws VerificationException {
        CloudsealPrincipal cloudsealPrincipal = new CloudsealPrincipal();
        cloudsealPrincipal.getRoles().add("ROLE_USER");
        cloudsealPrincipal.getRoles().add("ROLE_ADMIN");

        CloudsealAssertionAuthenticationToken token = new CloudsealAssertionAuthenticationToken("http://localhost:8080/saml/sp", "123", "SAMLResponse");
        when(cloudsealManager.getPublicKey()).thenReturn(publicKey);
        when(authResponseValidator.validateAuthResponse(any(PublicKey.class), anyString(), anyString(), anyString())).thenReturn(cloudsealPrincipal);
        classUnderTest.setCloudsealManager(cloudsealManager);
        classUnderTest.setResponseValidator(authResponseValidator);

        CloudsealAuthenticationToken authToken = (CloudsealAuthenticationToken) classUnderTest.authenticate(token);
        CloudsealUserDetails userDetails = (CloudsealUserDetails) authToken.getPrincipal();
        assertEquals(2, userDetails.getAuthorities().size());
        assertTrue(authToken.isAuthenticated());
        Collection<? extends GrantedAuthority> authorities = userDetails.getAuthorities();
        Iterator<? extends GrantedAuthority> it = authorities.iterator();
        int matchCount = 0;
        while (it.hasNext()) {
            GrantedAuthority authority = it.next();
            if (authority.getAuthority().equals("ROLE_USER") || authority.getAuthority().equals("ROLE_ADMIN")) {
                matchCount++;
            }
        }
        assertEquals(2, matchCount);
    }

    @Test
    public void testUnsuccessfulAuthentication() throws VerificationException {
        CloudsealAssertionAuthenticationToken token = new CloudsealAssertionAuthenticationToken("http://localhost:8080/saml/sp", "123", "SAMLResponse");
        when(cloudsealManager.getPublicKey()).thenReturn(publicKey);
        when(authResponseValidator.validateAuthResponse(any(PublicKey.class), anyString(), anyString(), anyString())).thenThrow(new VerificationException("oops"));
        classUnderTest.setCloudsealManager(cloudsealManager);
        classUnderTest.setResponseValidator(authResponseValidator);

        try {
            classUnderTest.authenticate(token);
            fail("expected error");
        } catch (BadCredentialsException ex) {
            assertEquals("Unable to verify response from Cloudseal IDP", ex.getMessage());
        }

    }
}
