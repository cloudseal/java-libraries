/* Copyright 2012 Cloudseal Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cloudseal.client.spring;

import com.cloudseal.client.saml2.IdpXmlParser;
import com.cloudseal.client.saml2.SamlBuilder;
import com.cloudseal.client.saml2.SamlBuilderImpl;
import com.cloudseal.client.saml2.SamlConfig;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.Resource;
import org.springframework.util.Assert;

import java.io.InputStream;
import java.security.PublicKey;

public class CloudsealManagerImpl implements InitializingBean, CloudsealManager {

    private Resource idpXml;
    private Resource keystore;
    private String keyName;
    private String keystorePassword;
    private String keyPassword;
    private String appId;

    private String ssoUrl;
    private PublicKey publicKey;

    private IdpXmlParser idpXmlParser;
    private SamlBuilder authRequestBuilder;

    public CloudsealManagerImpl() {
        idpXmlParser = new IdpXmlParser();
        authRequestBuilder = new SamlBuilderImpl();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(this.idpXml, "idpXml must be specified");
        Assert.notNull(this.keystore, "keystore must be specified");
        Assert.hasLength(this.keyName, "key must be specified");
        Assert.hasLength(this.keystorePassword, "keystorePassword must be specified");
        Assert.hasLength(this.keyPassword, "keyPassword must be specified");

        init();
    }

    private void init() throws Exception {
        InputStream idpIs = idpXml.getInputStream();
        byte[] idpData = IOUtils.toByteArray(idpIs);
        IOUtils.closeQuietly(idpIs);
        idpXmlParser.parse(idpData);
        this.ssoUrl = idpXmlParser.getSsoUrl();
        this.publicKey = idpXmlParser.getIdpPublicKey();

        InputStream keystoreIs = keystore.getInputStream();
        byte[] keystoreData = IOUtils.toByteArray(keystoreIs);
        IOUtils.closeQuietly(keystoreIs);

        SamlConfig authRequestContext = new SamlConfig();
        authRequestContext.setKeystore(keystoreData);
        authRequestContext.setKeyName(keyName);
        authRequestContext.setKeystorePassword(keystorePassword);
        authRequestContext.setKeyPassword(keyPassword);
        if (appId != null && appId.length() > 0) {
            authRequestContext.setProviderName(appId);
        }

        authRequestBuilder.init(authRequestContext);
    }

    @Override
    public String getSsoUrl() {
        return ssoUrl;
    }

    @Override
    public PublicKey getPublicKey() {
        return publicKey;
    }

    public void setIdpXml(Resource idpXml) {
        this.idpXml = idpXml;
    }

    public void setKeystore(Resource keystore) {
        this.keystore = keystore;
    }

    public void setKeyName(String keyName) {
        this.keyName = keyName;
    }

    public void setKeystorePassword(String keystorePassword) {
        this.keystorePassword = keystorePassword;
    }

    public void setKeyPassword(String keyPassword) {
        this.keyPassword = keyPassword;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    @Override
    public String generateSamlAuthRequest(String spIssuer, String acsUrl, String destination, String id) throws Exception {
        return authRequestBuilder.generateSamlAuthRequest(spIssuer, acsUrl, acsUrl, id);
    }
}
