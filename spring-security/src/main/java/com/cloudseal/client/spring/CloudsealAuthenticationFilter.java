/* Copyright 2012 Cloudseal Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cloudseal.client.spring;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The user will have been redirected to Cloudseal with a SAMLAuthRequest from the CloudsealEntryPoint. By now Cloudseal
 * will have authenticated the user (or not) and will pass him back (via a redirect or post) to his web app. It's the
 * responsibility of this filter to intercept the SAMLAuthResponse and construct a CloudsealAssertionAuthenticationToken which
 * can be passed to the CloudsealAuthenticationProvider via the AuthenticationManager property.
 */
public class CloudsealAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    public CloudsealAuthenticationFilter() {
        super("/cloudseal_acs");
    }

    public CloudsealAuthenticationFilter(String filterProcessesUrl) {
        super(filterProcessesUrl);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws AuthenticationException, IOException, ServletException {
        String requestId = (String) httpServletRequest.getSession().getAttribute(CloudsealEntryPoint.AUTH_REQUEST_ID);

        if (requestId == null || requestId.length() < 1) {
            throw new CloudsealResponseException("No SAML request ID could be found in the session");
        }

        String audience = (String) httpServletRequest.getSession().getAttribute(CloudsealEntryPoint.AUDIENCE);

        if (audience == null || audience.length() < 1) {
            throw new CloudsealResponseException("No SAML Audience could be found in the session");
        }

        String samlResponse = httpServletRequest.getParameter("SAMLResponse");

        if (samlResponse == null || samlResponse.length() < 1) {
            throw new CloudsealResponseException("No SAML response received from IDP");
        }

        CloudsealAssertionAuthenticationToken authenticationToken = new CloudsealAssertionAuthenticationToken(audience, requestId, samlResponse);
        return getAuthenticationManager().authenticate(authenticationToken);
    }

}
