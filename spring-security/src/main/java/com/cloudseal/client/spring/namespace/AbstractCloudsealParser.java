package com.cloudseal.client.spring.namespace;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.parsing.BeanComponentDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.xml.ParserContext;
import org.springframework.core.io.Resource;
import org.w3c.dom.Element;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.springframework.util.xml.DomUtils.getChildElementByTagName;

public abstract class AbstractCloudsealParser {

    private static final Logger LOGGER = Logger.getLogger(AbstractCloudsealParser.class);
    protected final ParserContext parserContext;
    protected final Element rootNode;

    public AbstractCloudsealParser(Element rootNode, ParserContext parserContext) {
        this.rootNode = rootNode;
        this.parserContext = parserContext;
    }

    protected BeanDefinition getExistingOrCreateBean(String existingId, Class<?> clazz, boolean register) {
        if (existingId != null && !existingId.isEmpty()) {
            BeanDefinitionRegistry registry = parserContext.getRegistry();
            if (registry.containsBeanDefinition(existingId)) {
                return registry.getBeanDefinition(existingId);
            }
        }
        if (register) {
            return createAndRegisterBean(clazz);
        }
        return createBean(clazz).getBeanDefinition();
    }

    protected String getAttribute(String name) {
        return rootNode.getAttribute(name);
    }

    protected int getBeanIndexToAddAfter(List list, String regexp) {
        int index = getBeanIndex(list, regexp);
        if (index == -1) {
            return 0;
        }
        return index + 1;
    }

    protected int getBeanIndex(List list, String regexp) {
        final Pattern pattern = Pattern.compile(regexp);
        final Matcher matcher = pattern.matcher("");
        int index = -1;
        for (int i = 0; i < list.size(); i++) {
            final Object item = list.get(i);
            if (BeanDefinition.class.isInstance(item)) {
                final BeanDefinition filter = (BeanDefinition) item;
                matcher.reset(filter.getBeanClassName());
                if (matcher.find()) {
                    index = i;
                }
            }
        }
        return index;
    }

    protected Element getRequiredElement(Element parentElement, String elementTag) {
        Element element = getChildElementByTagName(parentElement, elementTag);
        if (element == null) {
            throw createElementException(elementTag);
        }
        return element;
    }

    protected IllegalStateException createElementException(String elementTag) {
        return new IllegalStateException("Missing element in CloudSeal configuration: " + elementTag);
    }

    protected String getRequiredAttribute(Element element, String attributeTag) {
        String attribute = element.getAttribute(attributeTag);
        if (attribute.trim().isEmpty()) {
            throw new IllegalStateException("Missing or empty attribute of " + element.getNodeName() + " element "
                    + "in CloudSeal configuration: " + attributeTag);
        }
        return attribute;
    }

    protected BeanDefinition createAndRegisterBean(Class<?> beanClass) {
        return registerBean(createBean(beanClass));
    }

    protected BeanDefinitionBuilder createBean(Class<?> beanClass) {
        return BeanDefinitionBuilder.rootBeanDefinition(beanClass);
    }

    protected BeanDefinition registerBean(BeanDefinitionBuilder beanBuilder) {
        return registerBean(beanBuilder.getBeanDefinition());
    }

    protected BeanDefinition registerBean(BeanDefinition bean) {
        String beanID = parserContext.getReaderContext().generateBeanName(bean);
        return registerBean(bean, beanID);
    }

    protected BeanDefinition registerBean(BeanDefinitionBuilder beanBuilder, String beanID) {
        return registerBean(beanBuilder.getBeanDefinition(), beanID);
    }

    protected BeanDefinition registerBean(BeanDefinition bean, String beanID) {
        parserContext.registerBeanComponent(new BeanComponentDefinition(bean, beanID));
        return bean;
    }

    protected Resource getResourceFromLocation(String location) {
        Resource resource = parserContext.getReaderContext().getResourceLoader().getResource(location);
        if (!resource.exists()) {
            throw new IllegalStateException("Cannot find resource: " + location);
        }
        return resource;
    }

    protected File getFileFromLocation(String location) {
        File file;
        try {
            file = getResourceFromLocation(location).getFile();
        } catch (IOException e) {
            throw new IllegalStateException("Cannot obtain file from resource: " + location, e);
        }
        return file;
    }
}
