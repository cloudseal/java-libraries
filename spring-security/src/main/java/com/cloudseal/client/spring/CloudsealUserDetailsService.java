package com.cloudseal.client.spring;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Created with IntelliJ IDEA.
 * User: toby
 * Date: 19/11/2012
 * Time: 12:53
 * To change this template use File | Settings | File Templates.
 */
public interface CloudsealUserDetailsService extends UserDetailsService {

    public UserDetails loadUser(CloudsealUserDetails userDetails);

}
