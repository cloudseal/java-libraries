/* Copyright 2011 Cloudseal
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cloudseal.client.spring.namespace;

import com.cloudseal.client.spring.*;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.PropertyValue;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanReference;
import org.springframework.beans.factory.config.ConstructorArgumentValues;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.ManagedList;
import org.springframework.beans.factory.xml.ParserContext;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.w3c.dom.Element;

import java.util.List;

import static org.springframework.util.xml.DomUtils.getChildElementByTagName;

public class CloudsealBeanDefinitionParserInstance extends AbstractCloudsealParser {

    private static final String SPRING_AUTH_MANAGER_ID = "org.springframework.security.authenticationManager";
    private static final String ROOT_ENTRY_POINT_ID_ATTRIBUTE = "entry-point-id";
    private static final String ROOT_APP_ID_ATTRIBUTE = "app-id";
    private static final String ROOT_USER_DETAILS_SERVICE_REF_ATTRIBUTE = "user-details-service-ref";

    /*
     * Attributes below were added to inject custom beans to support routing saml request
     * to different IDP urls in Admin Console.
     */
    private static final String AUTHENTICATION_PROVIDER_NODE = "authentication-provider";
    private static final String AUTHENTICATION_PROVIDER_ID_ATTRIBUTE = "id";

    private static final String KEYSTORE_NODE = "keystore";
    private static final String LOCATION_ATTRIBUTE = "location";
    private static final String PASSWORD_ATTRIBUTE = "password";
    private static final String KEY = "key";
    private static final String NAME_ATTRIBUTE = "name";
    private static final String IDP_NODE = "idp";

    public CloudsealBeanDefinitionParserInstance(Element rootNode, ParserContext parserContext) {
        super(rootNode, parserContext);
        parse();
    }

    private void parse() {
        BeanDefinition cloudsealManager = createCloudsealManager();
        createAndRegisterEntryPoint(cloudsealManager);
        BeanDefinition authenticationProvider = createAuthenticationProvider(cloudsealManager);
        BeanDefinition authenticationManager = createOrUpdateAuthenticationManager(authenticationProvider);
        BeanDefinition cloudsealFilter = createCloudsealFilter(authenticationManager);
        registerBean(cloudsealFilter, "cloudsealFilter");
        BeanDefinition logoutFilter = createLogoutIfRequired(rootNode);
        createSecurityFilterChain(cloudsealFilter, logoutFilter);
    }

    private BeanDefinition createLogoutIfRequired(Element rootNode) {
        Element logout = getChildElementByTagName(rootNode, "logout");
        // If the developer didn't add a <logout /> element we don't create the logout handler and filter
        if (logout == null)
            return null;

        BeanDefinitionBuilder logoutHandler = createBean(CloudsealLogoutSuccessHandler.class);
        registerBean(logoutHandler, "cloudsealLogoutHandler");
        BeanDefinition logoutFilter = createLogoutFilter(logoutHandler.getBeanDefinition());
        registerBean(logoutFilter);
        return logoutFilter;
    }

    private BeanDefinition createCloudsealFilter(BeanDefinition authenticationManager) {
        BeanDefinitionBuilder builder = createBean(CloudsealAuthenticationFilter.class);
        builder.addPropertyValue("authenticationManager", authenticationManager);
        BeanDefinition cloudsealFilter = builder.getBeanDefinition();
        return cloudsealFilter;
    }

    private BeanDefinition createAuthenticationProvider(BeanDefinition cloudsealManager) {
        BeanDefinitionBuilder builder = createBean(CloudsealAuthenticationProvider.class);
        builder.addPropertyValue("cloudsealManager", cloudsealManager);

        // Check if the dev specified a custom UserDetailsService
        String userDetailsRef = getAttribute("user-details-service-ref");
        if (userDetailsRef != null && userDetailsRef.length() > 0) {
            builder.addPropertyReference("userDetailsService", userDetailsRef);
        }

        return builder.getBeanDefinition();
    }

    @SuppressWarnings("unchecked")
    private BeanDefinition createOrUpdateAuthenticationManager(BeanDefinition authenticationProvider) {
        Element element = getChildElementByTagName(rootNode, AUTHENTICATION_PROVIDER_NODE);
        if (element != null) {
            String id = getRequiredAttribute(element, AUTHENTICATION_PROVIDER_ID_ATTRIBUTE);
            if (!id.trim().isEmpty()) {
                registerBean(authenticationProvider, id);
            }
        }

        BeanDefinitionRegistry registry = parserContext.getRegistry();
        if (registry.containsBeanDefinition(SPRING_AUTH_MANAGER_ID)) {
            BeanDefinition bean = registry.getBeanDefinition(SPRING_AUTH_MANAGER_ID);
            MutablePropertyValues properties = bean.getPropertyValues();
            PropertyValue property = properties.getPropertyValue("providers");
            if (property == null) {
                List<BeanDefinition> list = new ManagedList<BeanDefinition>();
                list.add(authenticationProvider);
                properties.addPropertyValue("providers", list);
            } else {
                ((ManagedList<BeanDefinition>) property.getValue()).add(authenticationProvider);
            }
            return bean;
        }

        return createAuthenticationManager(authenticationProvider);
    }

    private BeanDefinition createAndRegisterEntryPoint(BeanDefinition cloudsealManager) {
        BeanDefinitionBuilder builder = createBean(CloudsealEntryPoint.class);
        builder.addPropertyValue("cloudsealManager", cloudsealManager);
        return registerBean(builder, getRequiredAttribute(rootNode, ROOT_ENTRY_POINT_ID_ATTRIBUTE));
    }

    private BeanDefinition createCloudsealManager() {
        Element keyManagerNode = getRequiredElement(rootNode, KEYSTORE_NODE);
        Element keyNode = getRequiredElement(keyManagerNode, KEY);
        Element idp = getRequiredElement(rootNode, IDP_NODE);
        String idpPath = getRequiredAttribute(idp, LOCATION_ATTRIBUTE);

        String location = getRequiredAttribute(keyManagerNode, LOCATION_ATTRIBUTE);
        String filePassword = getRequiredAttribute(keyManagerNode, PASSWORD_ATTRIBUTE);

        String appId = getAttribute("appId");

        BeanDefinitionBuilder builder = createBean(CloudsealManagerImpl.class);
        builder.addPropertyValue("keystore", location);
        builder.addPropertyValue("keystorePassword", filePassword);
        builder.addPropertyValue("idpXml", idpPath);
        builder.addPropertyValue("appId", appId);

        String key = getRequiredAttribute(keyNode, NAME_ATTRIBUTE);
        String password = getRequiredAttribute(keyNode, PASSWORD_ATTRIBUTE);
        builder.addPropertyValue("keyName", key);
        builder.addPropertyValue("keyPassword", password);

        return registerBean(builder);
    }

    private BeanDefinition createAuthenticationManager(BeanDefinition authenticationProvider) {
        List<BeanDefinition> list = new ManagedList<BeanDefinition>();
        list.add(authenticationProvider);
        BeanDefinitionBuilder builder = BeanDefinitionBuilder.rootBeanDefinition(ProviderManager.class);
        builder.addPropertyValue("providers", list);
        return registerBean(builder, SPRING_AUTH_MANAGER_ID);
    }

    private BeanDefinition createLogoutFilter(BeanDefinition logoutHandler) {
        // Create and register a SecurityContextLogoutHandler
        BeanDefinitionBuilder securityContextLogoutHandler = createBean(SecurityContextLogoutHandler.class);
        registerBean(securityContextLogoutHandler);
        // Create the logout filter with the cloudseal success handler and the SecurityContextLogoutHandler
        BeanDefinitionBuilder logoutFilter = createBean(LogoutFilter.class);
        logoutFilter.addConstructorArgValue(logoutHandler);
        logoutFilter.addConstructorArgValue(securityContextLogoutHandler.getBeanDefinition());
        logoutFilter.addPropertyValue("filterProcessesUrl", "/cloudseal_acs/logout.jsonp");
        return logoutFilter.getBeanDefinition();
    }

    private void createSecurityFilterChain(BeanDefinition cloudsealFilter, BeanDefinition logoutFilter) {
        // Obtain the filter chains and add the new chain to it
        BeanDefinition listFactoryBean = parserContext.getRegistry().getBeanDefinition(BeanIds.FILTER_CHAINS);
        List<BeanReference> filterChains = (List<BeanReference>)
                listFactoryBean.getPropertyValues().getPropertyValue("sourceList").getValue();

        BeanDefinition defaultFilterChain = parserContext.getRegistry().getBeanDefinition(filterChains.get(0).getBeanName());
        ConstructorArgumentValues constructorArgumentValues = defaultFilterChain.getConstructorArgumentValues();
        ManagedList currentFilters;
        currentFilters = (ManagedList) constructorArgumentValues.getIndexedArgumentValue(1, ManagedList.class).getValue();

        if (logoutFilter != null)
            currentFilters.add(0, logoutFilter);

        int index = getBeanIndexToAddAfter(currentFilters, "^org\\.springframework\\.security\\.web\\.context\\.");
        currentFilters.add(index, cloudsealFilter);
    }

}
