/* Copyright 2012 Cloudseal Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cloudseal.client.spring;

import org.springframework.security.authentication.AbstractAuthenticationToken;

/**
 * Acts as the bridge between the CloudsealAuthenticationFilter (which receives the SAMLResponse) and
 * the CloudsealAuthenticationProvider (which understands SAML)
 */
public class CloudsealAssertionAuthenticationToken extends AbstractAuthenticationToken {

    private TokenDetails details;

    public CloudsealAssertionAuthenticationToken(String audience, String requestId, String samlResponse) {
        super(null);
        details = new TokenDetails(audience, requestId, samlResponse);
    }

    @Override
    public Object getDetails() {
        return details;
    }

    @Override
    public Object getCredentials() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Object getPrincipal() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    class TokenDetails {
        private String samlResponse;
        private String requestId;
        private String audience;

        TokenDetails(String audience, String requestId, String samlResponse) {
            this.audience = audience;
            this.samlResponse = samlResponse;
            this.requestId = requestId;
        }

        public String getSamlResponse() {
            return samlResponse;
        }

        public String getRequestId() {
            return requestId;
        }

        public String getAudience() {
            return audience;
        }
    }
}
