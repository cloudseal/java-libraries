/* Copyright 2012 Cloudseal Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cloudseal.client.filter;

import static org.mockito.Mockito.*;
import static org.mockito.AdditionalMatchers.*;
import static org.junit.Assert.*;

import com.cloudseal.client.saml2.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.PublicKey;
import java.util.Arrays;

@RunWith(MockitoJUnitRunner.class)
public class Saml2FilterTests {

    private CloudsealFilter classUnderTest;

    @Mock private FilterConfig filterConfig;
    @Mock private ServletContext servletContext;
    @Mock private SamlBuilderImpl samlBuilder;
    @Mock private IdentifierGenerator identifierGenerator;
    @Mock private SamlValidatorImpl samlValidator;
    @Mock private IdpXmlParser idpXmlParser;
    @Mock private FileResolver fileResolver;
    @Mock private HttpServletRequest request;
    @Mock private HttpServletResponse response;
    @Mock private HttpSession session;

    @Before
    public void setup() {
        classUnderTest = new CloudsealFilter(samlBuilder, identifierGenerator,
                samlValidator, idpXmlParser, fileResolver);
        when(request.getSession()).thenReturn(session);
    }

    @Test
    public void testPreflight() throws ServletException {
        testValidation("keystorePath");

        when(filterConfig.getInitParameter("keystorePath")).thenReturn("keystorePath");
        testValidation("keystorePassword");

        when(filterConfig.getInitParameter("keystorePassword")).thenReturn("keystorePassword");
        testValidation("keyName");

        when(filterConfig.getInitParameter("keyName")).thenReturn("keyName");
        testValidation("keyPassword");

        when(filterConfig.getInitParameter("keyPassword")).thenReturn("keyPassword");
        testValidation("idpXmlPath");

        when(filterConfig.getInitParameter("idpXmlPath")).thenReturn("idpXmlPath");

        // expect no errors
        classUnderTest.preFlight(filterConfig);
    }

    @Test
    public void testInit() throws IOException, ServletException, Exception {
        when(filterConfig.getInitParameter("keystorePath")).thenReturn("keystorePath");
        when(filterConfig.getInitParameter("keystorePassword")).thenReturn("keystorePassword");
        when(filterConfig.getInitParameter("keyName")).thenReturn("keyName");
        when(filterConfig.getInitParameter("keyPassword")).thenReturn("keyPassword");
        when(filterConfig.getInitParameter("idpXmlPath")).thenReturn("idpXmlPath");
        when(filterConfig.getInitParameter("appId")).thenReturn("appId");

        when(fileResolver.getFile(any(ServletContext.class), eq("keystorePath"))).thenReturn(new byte[]{0x1});
        when(fileResolver.getFile(any(ServletContext.class), eq("idpXmlPath"))).thenReturn(new byte[]{0x2});
        classUnderTest.init(filterConfig);

        verify(samlBuilder).init(argThat(new AuthRequestContextMatcher()));
        verify(idpXmlParser).parse(aryEq(new byte[]{0x2}));

        // Test failure to find keystore
        reset(fileResolver);
        when(fileResolver.getFile(any(ServletContext.class), eq("keystorePath"))).thenThrow(new IOException());
        try {
            classUnderTest.init(filterConfig);
            fail("expected nested IOException");
        } catch (ServletException ex) {

        }
    }

    class AuthRequestContextMatcher extends ArgumentMatcher<SamlConfig> {
        @Override
        public boolean matches(Object o) {
            SamlConfig context = (SamlConfig) o;
            // should match the first (and only) byte in the mocked byte array
            assertTrue(Arrays.equals(new byte[]{0x1}, context.getKeystore()));
            assertEquals("appId", context.getProviderName());
            return true;
        }
    }

    private void testValidation(String expectedFailure) {
        try {
            classUnderTest.preFlight(filterConfig);
            fail("expected error");
        } catch (ServletException ex) {
            assertEquals("initParam: " + expectedFailure + " must be set", ex.getMessage());
        }
    }

    @Test
    public void testAuthenticate() throws Exception {
        when(identifierGenerator.generateIdentifier()).thenReturn("123");
        when(request.getHeader("Host")).thenReturn("localhost:8080");
        when(request.getRequestURI()).thenReturn("/testapp");
        when(samlBuilder.generateSamlAuthRequest(anyString(), anyString(), anyString(), anyString())).thenReturn("SAMLRequest=x");
        when(idpXmlParser.getSsoUrl()).thenReturn("https://test.cloudseal.com/saml");
        classUnderTest.authenticate(request, response);

        verify(session).setAttribute(CloudsealFilter.SAML2_ID_ATTRIBUTE, "123");
        verify(session).setAttribute(eq(CloudsealFilter.SAVED_REQUEST), any(SavedRequest.class));
        verify(session).setAttribute(CloudsealFilter.SAML2_AUDIENCE_ATTRIBUTE, "http://localhost:8080/saml/sp");
        verify(samlBuilder).generateSamlAuthRequest("http://localhost:8080/saml/sp", "http://localhost:8080/testapp", "http://localhost:8080/testapp", "123");
        verify(response).sendRedirect("https://test.cloudseal.com/saml?SAMLRequest=x");

        // Test https url
        when(request.isSecure()).thenReturn(true);
        classUnderTest.authenticate(request, response);
        verify(samlBuilder).generateSamlAuthRequest("http://localhost:8080/saml/sp", "https://localhost:8080/testapp", "https://localhost:8080/testapp", "123");

        // Verify exceptions
        reset(response);
        reset(samlValidator);
        when(samlBuilder.generateSamlAuthRequest(anyString(), anyString(), anyString(), anyString())).thenThrow(new Exception());
        try {
            classUnderTest.authenticate(request, response);
            fail("expected error");
        } catch (ServletException ex) {
            // ok
        }
        verify(response, never()).sendRedirect(anyString());
        verify(session).removeAttribute(CloudsealFilter.SAVED_REQUEST);
        verify(session).removeAttribute(CloudsealFilter.SAML2_ID_ATTRIBUTE);
        verify(session).removeAttribute(CloudsealFilter.SAML2_AUDIENCE_ATTRIBUTE);
    }



    @Test
    public void testVerifyResponse() throws VerificationException, IOException, ServletException {
        CloudsealPrincipal expectedPrincipal = new CloudsealPrincipal();
        PublicKey publicKey = Mockito.mock(PublicKey.class);
        when(request.getParameter("SAMLResponse")).thenReturn("SAMLResponse");
        when(session.getAttribute(CloudsealFilter.SAML2_ID_ATTRIBUTE)).thenReturn("123");
        when(session.getAttribute(CloudsealFilter.SAML2_AUDIENCE_ATTRIBUTE)).thenReturn("http://localhost:8080/saml/sp");
        when(idpXmlParser.getIdpPublicKey()).thenReturn(publicKey);
        when(samlValidator.validateAuthResponse(eq(publicKey), eq("SAMLResponse"), eq("123"), eq("http://localhost:8080/saml/sp"))).thenReturn(expectedPrincipal);

        CloudsealPrincipal actualPrincipal = classUnderTest.verifyResponse(request);
        assertTrue(expectedPrincipal == actualPrincipal);
        verify(idpXmlParser).getIdpPublicKey();
        verify(session).removeAttribute(CloudsealFilter.SAML2_ID_ATTRIBUTE);
        verify(session).removeAttribute(CloudsealFilter.SAML2_AUDIENCE_ATTRIBUTE);

        // Test missing request ID
        when(session.getAttribute(CloudsealFilter.SAML2_ID_ATTRIBUTE)).thenReturn(null);
        try {
            classUnderTest.verifyResponse(request);
            fail("expect error");
        } catch (ServletException ex) {
            assertEquals("Unable to retrieve SAML request ID from session", ex.getMessage());
        }

        // Test missing SAML response
        reset(request);
        when(request.getParameter("SAMLResponse")).thenReturn(null);
        try {
            classUnderTest.verifyResponse(request);
            fail("expect error");
        } catch (ServletException ex) {
            assertEquals("Unable to verify response from Cloudseal servers", ex.getMessage());
        }
    }

    @Test
    public void testHackedResponse() throws VerificationException, IOException, ServletException {
        PublicKey publicKey = Mockito.mock(PublicKey.class);

        when(request.getParameter("SAMLResponse")).thenReturn("SAMLResponse");
        when(session.getAttribute(CloudsealFilter.SAML2_ID_ATTRIBUTE)).thenReturn("123");
        when(session.getAttribute(CloudsealFilter.SAML2_AUDIENCE_ATTRIBUTE)).thenReturn("http://localhost:8080/saml/sp");
        when(idpXmlParser.getIdpPublicKey()).thenReturn(publicKey);
        when(samlValidator.validateAuthResponse(eq(publicKey), eq("SAMLResponse"), eq("123"), eq("http://localhost:8080/saml/sp"))).thenThrow(new VerificationException("oops"));

        try {
            classUnderTest.verifyResponse(request);
            fail("expect exception");
        } catch (ServletException ex) {

        }
        verify(idpXmlParser).getIdpPublicKey();
        verify(session).removeAttribute(CloudsealFilter.SAML2_ID_ATTRIBUTE);
        verify(session).removeAttribute(CloudsealFilter.SAML2_AUDIENCE_ATTRIBUTE);
    }

    @Test
    public void testRedirect() throws IOException {
        CloudsealPrincipal cloudsealPrincipal = new CloudsealPrincipal();
        SavedRequest savedRequest = Mockito.mock(SavedRequest.class);

        when(savedRequest.getRedirectUrl()).thenReturn("http://somewhere.com/someurl");
        when(session.getAttribute(CloudsealFilter.SAVED_REQUEST)).thenReturn(savedRequest);
        when(session.getAttribute(ServletConstants.CLOUDSEAL_PRINCIPAL)).thenReturn(cloudsealPrincipal);

        classUnderTest.redirect(request, response);
        verify(session).setAttribute("authenticated", Boolean.TRUE);
        verify(session).setAttribute(ServletConstants.CLOUDSEAL_PRINCIPAL, cloudsealPrincipal);
        verify(response).sendRedirect("http://somewhere.com/someurl");
    }

    @Test
    public void testPost() throws Exception {
        CloudsealPrincipal cloudsealPrincipal = new CloudsealPrincipal();
        SavedRequest savedRequest = Mockito.mock(SavedRequest.class);
        PrintWriter writer = Mockito.mock(PrintWriter.class);

        when(response.getWriter()).thenReturn(writer);
        when(savedRequest.getPostBody()).thenReturn("post body");
        when(session.getAttribute(CloudsealFilter.SAVED_REQUEST)).thenReturn(savedRequest);
        when(session.getAttribute(ServletConstants.CLOUDSEAL_PRINCIPAL)).thenReturn(cloudsealPrincipal);

        classUnderTest.post(request, response);
        verify(session).setAttribute("authenticated", Boolean.TRUE);
        verify(session).setAttribute(ServletConstants.CLOUDSEAL_PRINCIPAL, cloudsealPrincipal);
        verify(writer).print("post body");
        verify(writer).flush();
    }

    @Test
    public void testDoFilterAuthenticated() throws IOException, ServletException {
        FilterChain chain = Mockito.mock(FilterChain.class);

        when(session.getAttribute("authenticated")).thenReturn(Boolean.TRUE);
        when(request.getRequestURI()).thenReturn("not logout url");

        classUnderTest.doFilter(request, response, chain);

        verify(request).getSession();
        verify(session).getAttribute("authenticated");
        verify(request).getRequestURI();

        verify(chain).doFilter(request, response);
        verifyNoMoreInteractions(request);
        verifyNoMoreInteractions(session);
        verifyZeroInteractions(response);
    }

    @Test
    public void testDoFilterNotAuthenticated() throws IOException, ServletException {
        FilterChain chain = Mockito.mock(FilterChain.class);

        when(session.getAttribute("authenticated")).thenReturn(null);
        when(request.getRequestURI()).thenReturn("not logout url");
        when(request.getParameter("SAMLResponse")).thenReturn(null);
        CloudsealFilter spy = spy(classUnderTest);

        spy.doFilter(request, response, chain);

        verify(request, atLeastOnce()).getSession();
        verify(session).getAttribute("authenticated");
        verify(request, atLeastOnce()).getRequestURI();

        verify(spy).authenticate(any(HttpServletRequest.class), any(HttpServletResponse.class));
        verify(session, never()).setAttribute(eq("authenticated"), eq(Boolean.TRUE));
        verify(session, never()).setAttribute(eq(ServletConstants.CLOUDSEAL_PRINCIPAL), any(CloudsealPrincipal.class));
    }

    @Test
    public void testDoFilterSAMLResponseGET() throws Exception {
        CloudsealPrincipal principal = new CloudsealPrincipal();

        FilterChain chain = Mockito.mock(FilterChain.class);

        when(session.getAttribute("authenticated")).thenReturn(null);
        when(request.getRequestURI()).thenReturn("not logout url");
        when(request.getParameter("SAMLResponse")).thenReturn("ResponsePresent");
        when(request.getMethod()).thenReturn("GET");
        SavedRequest savedRequest = new SavedRequest(request);
        when(session.getAttribute(CloudsealFilter.SAVED_REQUEST)).thenReturn(savedRequest);
        CloudsealFilter spy = spy(classUnderTest);
        doReturn(principal).when(spy).verifyResponse(request);
        doNothing().when(spy).redirect(request, response);

        spy.doFilter(request, response, chain);

        verify(session).setAttribute(ServletConstants.CLOUDSEAL_PRINCIPAL, principal);
        verify(spy).redirect(request, response);
    }

    @Test
    public void testDoFilterSAMLResponsePOST() throws Exception {
        CloudsealPrincipal principal = new CloudsealPrincipal();

        FilterChain chain = Mockito.mock(FilterChain.class);

        when(session.getAttribute("authenticated")).thenReturn(null);
        when(request.getRequestURI()).thenReturn("not logout url");
        when(request.getParameter("SAMLResponse")).thenReturn("ResponsePresent");
        when(request.getMethod()).thenReturn("POST");
        SavedRequest savedRequest = new SavedRequest(request);
        when(session.getAttribute(CloudsealFilter.SAVED_REQUEST)).thenReturn(savedRequest);
        CloudsealFilter spy = spy(classUnderTest);
        doReturn(principal).when(spy).verifyResponse(request);
        doNothing().when(spy).post(request, response);

        spy.doFilter(request, response, chain);

        verify(session).setAttribute(ServletConstants.CLOUDSEAL_PRINCIPAL, principal);
        verify(spy).post(request, response);
    }

    @Test
    public void testDoFilterNoCloudsealUser() throws Exception {
        FilterChain chain = Mockito.mock(FilterChain.class);

        when(session.getAttribute("authenticated")).thenReturn(null);
        when(request.getRequestURI()).thenReturn("not logout url");
        when(request.getParameter("SAMLResponse")).thenReturn("ResponsePresent");
        CloudsealFilter spy = spy(classUnderTest);
        doReturn(null).when(spy).verifyResponse(request);

        spy.doFilter(request, response, chain);

        verify(session, never()).setAttribute(eq(ServletConstants.CLOUDSEAL_PRINCIPAL), any(CloudsealPrincipal.class));
        verify(response).sendError(403, "Invalid SAML Response");
    }


}
