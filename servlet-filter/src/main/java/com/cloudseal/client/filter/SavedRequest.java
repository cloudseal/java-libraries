/* Copyright 2012 Cloudseal Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cloudseal.client.filter;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class SavedRequest implements Serializable {

    private final String protocol;
    private final String host;
    private final String requestUri;
    private final boolean isPost;
    private final Map<String, String[]> parameters;

    public SavedRequest(HttpServletRequest request) {
        parameters = new LinkedHashMap<String, String[]>();
        if (request.isSecure()) {
            protocol = "https//";
        } else {
            protocol = "http://";
        }

        if ("POST".equals(request.getMethod())) {
            isPost = true;
        } else {
            isPost = false;
        }

        host = request.getHeader("Host");
        requestUri = request.getRequestURI();

        Set<String> keys = request.getParameterMap().keySet();
        for (String key: keys) {
            String[] value = request.getParameterValues(key);
            parameters.put(key, value);
        }
    }

    public boolean isPost() {
        return isPost;
    }

    public String getRedirectUrl() {
        StringBuilder buffer = new StringBuilder();
        buffer.append(protocol);
        buffer.append(host);
        buffer.append(requestUri);

        boolean first = true;
        Set<String> keys = parameters.keySet();
        for (String key: keys) {
            String value = parameters.get(key)[0];
            String queryToken = first ? "?" : "&";
            buffer.append(queryToken);
            buffer.append(key);
            if (value != null && value.length() > 0) {
                buffer.append("=");
                buffer.append(value);
            }
            first = false;
        }

        return buffer.toString();
    }

    public String getPostBody() {
        Set<String> keys = parameters.keySet();

        StringBuilder url = new StringBuilder();
        url.append(protocol);
        url.append(host);
        url.append(requestUri);

        StringBuilder builder = new StringBuilder();
        builder.append("<html>");
        builder.append("<body>");
        builder.append("<form action=\"" + url.toString() + "\" method=\"post\">");
        for (String key : keys) {
            String[] values = parameters.get(key);
            for (String value : values) {
                builder.append("<input type=\"hidden\" name=\"" + key + "\" value=\"" + value + "\" />");
            }
        }
        builder.append("</form>");
        builder.append(buildJavascriptStatement());
        builder.append("</body>");
        builder.append("</html>");

        return builder.toString();
    }

    private String buildJavascriptStatement() {
        StringBuilder builder = new StringBuilder();
        builder.append("<script type=\"text/javascript\">");
        builder.append("document.forms[0].submit();");
        builder.append("</script>");
        return builder.toString();
    }

}
