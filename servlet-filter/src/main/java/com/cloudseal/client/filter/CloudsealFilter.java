/* Copyright 2012 Cloudseal Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cloudseal.client.filter;

import static com.cloudseal.client.filter.ServletConstants.CLOUDSEAL_PRINCIPAL;

import com.cloudseal.client.saml2.*;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.Date;

public class CloudsealFilter implements Filter {

    public static final String SAML2_ID_ATTRIBUTE = "com.cloudseal.client.filter.Saml2Filter.Id";
    public static final String SAML2_AUDIENCE_ATTRIBUTE = "com.cloudseal.client.filter.Saml2Filter.Audience";
    public static final String SAVED_REQUEST = "com.cloudseal.client.filter.Saml2Filter.SavedRequest";

    private static final Logger log = Logger.getLogger(CloudsealFilter.class);
    private static final String BROWSER_INITIATED_SLO_URL = "/logout.jsonp";
    private static final int DEFAULT_BUFFER_SIZE = 1024;

    private final SamlBuilder samlRequestBuilder;
    private final IdentifierGenerator identifierGenerator;
    private final SamlValidator samlValidator;
    private final IdpXmlParser idpXmlParser;
    private final FileResolver fileResolver;

    public CloudsealFilter() {
        samlRequestBuilder = new SamlBuilderImpl();
        identifierGenerator = new IdentifierGenerator();
        samlValidator = new SamlValidatorImpl();
        idpXmlParser = new IdpXmlParser();
        fileResolver = new FileResolver();
    }

    CloudsealFilter(SamlBuilderImpl samlRequestBuilder, IdentifierGenerator identifierGenerator,
                    SamlValidatorImpl samlValidator, IdpXmlParser idpXmlParser,
                    FileResolver fileResolver) {
        this.samlRequestBuilder = samlRequestBuilder;
        this.identifierGenerator = identifierGenerator;
        this.samlValidator = samlValidator;
        this.idpXmlParser = idpXmlParser;
        this.fileResolver = fileResolver;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        synchronized (this) {
            preFlight(filterConfig);

            String keystorePath = filterConfig.getInitParameter("keystorePath");
            String keystorePassword = filterConfig.getInitParameter("keystorePassword");
            String keyName = filterConfig.getInitParameter("keyName");
            String keyPassword = filterConfig.getInitParameter("keyPassword");
            String idpPath = filterConfig.getInitParameter("idpXmlPath");
            String appId = filterConfig.getInitParameter("appId");

            SamlConfig context = new SamlConfig();
            context.setKeystorePassword(keystorePassword);
            context.setKeyName(keyName);
            context.setKeyPassword(keyPassword);

            if (! empty(appId)) {
                context.setProviderName(appId);
            }

            try {
                byte[] keystore = fileResolver.getFile(filterConfig.getServletContext(), keystorePath);
                byte[] idp = fileResolver.getFile(filterConfig.getServletContext(), idpPath);
                context.setKeystore(keystore);
                samlRequestBuilder.init(context);
                idpXmlParser.parse(idp);
            } catch (MissingKeyException ex) {
                throw new ServletException(ex.getMessage());
            } catch (Exception ex) {
                throw new ServletException(ex);
            }
        }
    }

    void preFlight(FilterConfig filterConfig) throws ServletException {
        String keystorePath = filterConfig.getInitParameter("keystorePath");
        String keystorePassword = filterConfig.getInitParameter("keystorePassword");
        String keyName = filterConfig.getInitParameter("keyName");
        String keyPassword = filterConfig.getInitParameter("keyPassword");
        String idpPath = filterConfig.getInitParameter("idpXmlPath");

        validateParam("keystorePath", keystorePath);
        validateParam("keystorePassword", keystorePassword);
        validateParam("keyName", keyName);
        validateParam("keyPassword", keyPassword);
        validateParam("idpXmlPath", idpPath);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        if (Boolean.TRUE.equals(httpRequest.getSession().getAttribute("authenticated"))) {
            if (httpRequest.getRequestURI().endsWith(BROWSER_INITIATED_SLO_URL)) {
                try {
                    // logout request
                    samlValidator.validateLogoutRequest(idpXmlParser.getIdpPublicKey(), ((HttpServletRequest) request).getQueryString());
                    httpRequest.getSession().removeAttribute("authenticated");
                    httpRequest.getSession().invalidate();
                    String callback = request.getParameter("callback");
                    sendLogoutResponse(callback, httpResponse);
                    return;
                } catch (VerificationException ex) {
                    throw new ServletException("Unable to verify authenticity of LogoutRequest");
                }
            }
            chain.doFilter(httpRequest, response);
            return;
        }

        else {
            if (httpRequest.getRequestURI().endsWith(BROWSER_INITIATED_SLO_URL)) {
                // logout request
                String callback = request.getParameter("callback");
                sendLogoutResponse(callback, httpResponse);
                return;
            }
        }

        if (samlResponsePresent(httpRequest)) {
            CloudsealPrincipal cloudsealPrincipal = verifyResponse(httpRequest);
            if (cloudsealPrincipal != null) {
                SavedRequest savedRequest = (SavedRequest) httpRequest.getSession().getAttribute(SAVED_REQUEST);
                httpRequest.getSession().setAttribute(CLOUDSEAL_PRINCIPAL, cloudsealPrincipal);
                if (savedRequest.isPost()) {
                    post(httpRequest, httpResponse);
                    return;
                }
                else {
                    redirect(httpRequest, httpResponse);
                    return;
                }
            } else {
                ((HttpServletResponse) response).sendError(403, "Invalid SAML Response");
                return;
            }
        } else {
            authenticate(httpRequest, httpResponse);
        }
    }

    private void sendLogoutResponse(String callback, HttpServletResponse response) throws IOException {
        response.setContentType("application/javascript");
        String jsonp = String.format("%s({\"status\":\"success\"})", callback);
        response.getWriter().write(jsonp);
        response.getWriter().flush();
    }

    private void validateParam(String key, String value) throws ServletException {
        if (empty(value))
            throw new ServletException("initParam: " + key + " must be set");
    }

    private boolean empty(String text) {
        boolean textPresent = text != null && text.length() > 0 && text.matches("[^\\s]+");
        return ! textPresent;
    }

    void authenticate(HttpServletRequest httpRequest, HttpServletResponse response) throws ServletException {
        try {
            String id = identifierGenerator.generateIdentifier();

            // we will check the inResponseTo against this
            httpRequest.getSession().setAttribute(SAML2_ID_ATTRIBUTE, id);

            SavedRequest savedRequest = new SavedRequest(httpRequest);
            httpRequest.getSession().setAttribute(SAVED_REQUEST, savedRequest);

            String acsUrl = buildAcsUrl(httpRequest);
            String spIssuer = buildSpIssuer(httpRequest);
            httpRequest.getSession().setAttribute(SAML2_AUDIENCE_ATTRIBUTE, spIssuer);

            String samlAuthRequest = samlRequestBuilder.generateSamlAuthRequest(spIssuer, acsUrl, acsUrl, id);
            String redirectUrl = idpXmlParser.getSsoUrl() + "?" + samlAuthRequest;
            response.sendRedirect(redirectUrl);
        } catch (Exception ex) {
            httpRequest.getSession().removeAttribute(SAVED_REQUEST);
            httpRequest.getSession().removeAttribute(SAML2_ID_ATTRIBUTE);
            httpRequest.getSession().removeAttribute(SAML2_AUDIENCE_ATTRIBUTE);
            throw new ServletException(ex);
        }
    }

    private String buildAcsUrl(HttpServletRequest request) {
        StringBuilder builder = new StringBuilder();
        if (request.isSecure() || "https".equals(request.getScheme())
                || "https".equals(request.getHeader("X-Forwarded-Proto"))) {
            builder.append("https://");
        } else {
            builder.append("http://");
        }
        builder.append(request.getHeader("Host"));
        builder.append(request.getContextPath());
        builder.append("/cloudseal_acs");
        return builder.toString();
    }

    CloudsealPrincipal verifyResponse(HttpServletRequest request) throws ServletException, IOException {
        String samlResponse = request.getParameter("SAMLResponse");
        if (samlResponse == null || samlResponse.length() < 1) {
           throw new ServletException("Unable to verify response from Cloudseal servers");
        }
        try {
            String expectedInResponseTo = (String) request.getSession().getAttribute(SAML2_ID_ATTRIBUTE);
            String expectedAudience = (String) request.getSession().getAttribute(SAML2_AUDIENCE_ATTRIBUTE);
            if (expectedInResponseTo == null || expectedInResponseTo.length() < 1)
                throw new ServletException("Unable to retrieve SAML request ID from session");

            CloudsealPrincipal cloudsealPrincipal = samlValidator.validateAuthResponse(idpXmlParser.getIdpPublicKey(), samlResponse, expectedInResponseTo, expectedAudience);
            return cloudsealPrincipal;
        } catch (VerificationException ex) {
            throw new ServletException(ex);
        } finally {
            request.getSession().removeAttribute(SAML2_ID_ATTRIBUTE);
            request.getSession().removeAttribute(SAML2_AUDIENCE_ATTRIBUTE);
        }
    }

    private boolean samlResponsePresent(HttpServletRequest httpRequest) {
        return httpRequest.getParameter("SAMLResponse") != null
                && httpRequest.getParameter("SAMLResponse").length() > 0;
    }

    private String buildSpIssuer(HttpServletRequest httpRequest) {
        StringBuilder builder = new StringBuilder("http://");
        String host = httpRequest.getHeader("Host");
        if (host == null || host.length() < 1) {
            host = "www.mycompany.com";
        }
        builder.append(host);
        builder.append("/saml/sp");
        return builder.toString();
    }

    void redirect(HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws IOException {
        // Prevent session  fixation - get the attributes from the session then kill it and recreate a new one
        SavedRequest savedRequest = (SavedRequest) httpRequest.getSession().getAttribute(SAVED_REQUEST);
        httpRequest.getSession().removeAttribute(SAVED_REQUEST);
        String redirectUrl = savedRequest.getRedirectUrl();
        CloudsealPrincipal cloudsealPrincipal = (CloudsealPrincipal) httpRequest.getSession().getAttribute(CLOUDSEAL_PRINCIPAL);

        httpRequest.getSession().invalidate();
        httpRequest.getSession().setAttribute("authenticated", Boolean.TRUE);
        httpRequest.getSession().setAttribute(CLOUDSEAL_PRINCIPAL, cloudsealPrincipal);
        httpResponse.sendRedirect(redirectUrl);
    }

    void post(HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws IOException {
        // Prevent session  fixation - get the attributes from the session then kill it and recreate a new one
        SavedRequest savedRequest = (SavedRequest) httpRequest.getSession().getAttribute(SAVED_REQUEST);
        httpRequest.getSession().removeAttribute(SAVED_REQUEST);
        CloudsealPrincipal cloudsealPrincipal = (CloudsealPrincipal) httpRequest.getSession().getAttribute(CLOUDSEAL_PRINCIPAL);

        httpRequest.getSession().invalidate();
        httpRequest.getSession().setAttribute("authenticated", Boolean.TRUE);
        httpRequest.getSession().setAttribute(CLOUDSEAL_PRINCIPAL, cloudsealPrincipal);

        String postBody = savedRequest.getPostBody();
        httpResponse.getWriter().print(postBody);
        httpResponse.getWriter().flush();
    }

    protected void preventCaching(HttpServletResponse response) {
        Date now = new Date();
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Date", now.getTime());
        response.setDateHeader("Expires", now.getTime() - 86400000L); // one day old
        response.setHeader("Cache-Control", "no-cache");
        response.addHeader("Cache-Control", "no-store");
        response.addHeader("Cache-Control", "must-revalidate");
    }

    private void writeContent(HttpServletResponse response, String contentType, InputStream content) throws IOException {
        response.setBufferSize(DEFAULT_BUFFER_SIZE);
        response.setContentType(contentType);

        BufferedInputStream input = null;
        BufferedOutputStream output = null;

        try {
            input = new BufferedInputStream(content, DEFAULT_BUFFER_SIZE);
            output = new BufferedOutputStream(response.getOutputStream(), DEFAULT_BUFFER_SIZE);

            byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
            int length = 0;
            while ((length = input.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }
        } finally {
            output.close();
            input.close();
        }
    }

    @Override
    public void destroy() {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
