/* Copyright 2012 Cloudseal Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cloudseal.client.filter;

import javax.servlet.ServletContext;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class FileResolver {

    byte[] getFile(ServletContext servletContext, String path) throws IOException {
        InputStream is;
        if (path.startsWith("classpath:")) {
            String[] tokens = path.split("classpath:");
            String keystoreLocation = tokens[1];
            is = getClass().getClassLoader().getResourceAsStream(keystoreLocation);
        } else {
            is = servletContext.getResourceAsStream(path);
        }

        if (is == null) {
            throw new FileNotFoundException(path);
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        byte[] b = new byte[100];
        int read;
        while ((read = is.read(b)) != -1) {
            bos.write(b, 0, read);
        }

        return bos.toByteArray();
    }
}
