package com.cloudseal.client.tomcat;

import com.cloudseal.client.saml2.CloudsealPrincipal;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: toby
 * Date: 29/11/2012
 * Time: 15:10
 * To change this template use File | Settings | File Templates.
 */
public class CloudsealDemoServlet extends HttpServlet {

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CloudsealPrincipal principal = (CloudsealPrincipal) request.getUserPrincipal();
        request.setAttribute("username", principal.getUsername());
        request.setAttribute("firstName", principal.getFirstName());
        request.setAttribute("lastName", principal.getLastName());
        request.setAttribute("email", principal.getEmail());
        getServletContext().getRequestDispatcher("/WEB-INF/jsp/demo.jsp").forward(request, response);
    }
}
