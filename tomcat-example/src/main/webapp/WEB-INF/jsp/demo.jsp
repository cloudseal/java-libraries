<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Demo servlet</title>
</head>
<body>
<h1>Demo servlet</h1>
<p>Hi ${firstName}</p>
<p>This is what I know about you:</p>
<ul>
    <li><strong>Username:</strong> ${username}</li>
    <li><strong>First name:</strong>  ${firstName}</li>
    <li><strong>Last name:</strong> ${lastName}</li>
    <li><strong>Email:</strong> ${email}</li>
</ul>
</body>
</html>
